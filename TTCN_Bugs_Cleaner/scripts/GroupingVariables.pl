#!/usr/bin/perl
#
# The script checks if variables of the same type are groupped together.
#
# rev. PA1
#
# usage: ./ttcngroupvars.pl in.ttcn

# check if an argument was given
if (@ARGV == 0)
{
    print "Error:\n";
	print "Input TD filename must be specified!";
    exit;
}
else
{   print "Lines:\n";
}
# open file
open (my $in, "<", $ARGV[0]) or die "$ARGV[0] <- ",$!;

# declare
my $i = 0;
my @types;
my $result = 0;    # 0 - success, 1 - fail
my $bracket = 0;
my $inside = 0;
my @errors;
my @tc_name;
my $errors_count = 0;

# subroutines
sub check_brackets($)
#--------------------------------------------------------------#
# This subroutine checks if there are any curly brackets
# in the line and increases/decreases number of them 
# where necessary.
# It also prints the result at the end of a testcase/function.
#--------------------------------------------------------------#
{
	my $string = shift;
	# check if a line contains curly brackets
	if (($string =~ m/.*{.*}.*/i || $string =~ m/.*}.*{.*/i) && $inside == 1)
	{
		# uncomment for debugging
		#print "line $i: number of brackets did not change ($bracket)\n";
		;
	}
	elsif ($string =~ m/.*{.*/i && $inside == 1)
	{
		$bracket++;
		# uncomment for debugging
		#print "line $i: number of brackets after increase: $bracket\n";
	}
	elsif ($string =~ m/.*}.*/i && $inside == 1)
	{
		$bracket--;
		# uncomment for debugging
		#print "line $i: number of brackets after decrease: $bracket\n";
		if ($bracket == 0)
		{
			$inside = 0;
			#print "line $i: End\n";
			if ($result == 0)
			{
				#print "Result OK...\n";
			}
			else
			{
			}
		}
	}
}

# read file
while (my $line = <$in>)
{
	# step up line counter
	$i++;
	
	check_brackets($line);
		
	# start of a new testcase/function
	if ($line =~ m/^#{0}\s*testcase\s+(tc_\w+)/i || $line =~ m/^#{0}\s*function\s+(\w+)/i)
	{
		if ($line =~ m/^#{0}\s*testcase\s+(tc_\w+)/i)
		{
			@tc_name = split('_',$1);
		}

		@types = ("");
		$result = 0;
		$inside = 1;
		check_brackets($line);
	}
	
	# consider variable declaration only
	if ($line =~ m/^\s*var\s+(\w+)\s+(\w+)(?!.*create)/i && $inside == 1)
	{
		# uncomment for debugging
		#print "$i: $line";
		#print "var type is: '$1'\n";

		# counter for foreach loop
		my $j = 0;
			
		# check if variable type read is present in a table
		foreach (@types)
		{
			# uncomment for debugging
			#print "\$_ = $_\n";
			#print "\$1 = $1\n";
			
			# if type already exists in @types
			if ($_ eq $1)
			{
				# and if it is not the last item on the list
				if ($types[$#types] ne $1)
				{
				    print "$i Group variables type $1\n";
					$result = 1;
					push @errors, $i;
				}
					
				# uncomment for debugging
				#print "Table content: @types\n";
					
				#exit;
			}
			else
			{
				# uncomment for debugging
				#print "Grupowanie wporzo :P\n";
				#print "\$\#types: $#types, \$j: $j\n";
				
				# if this is the last item on the list	
				if ($#types == $j)
				{
					if ($result == 0) # and the result is OK (current type was not put into the table previously)
					{
						# add type to the table
						push @types, $1;
						
						# uncomment for debugging
						#print "Table content after push: @types\n";
					}	
					last;
				}		
			}
			# step up loop counter
			$j++;
		}

	}
	
}

close $in;

# error summary
$error_count = $#errors + 1;
#print "Lines:\n";
#if ($error_count > 0)
#{
#	foreach (@errors)
#	{
#		$errors_count++;
#		print "$_\n";
#
#	}
#}

exit;