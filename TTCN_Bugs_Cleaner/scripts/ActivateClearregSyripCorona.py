
# This scripts determines if SYRIP and CORONA checks are performed before and after every testcase in functions
# f_Check_Before and f_Check_After respectively. The argument of the script is .ttcn file.

from sys import argv
from TestCasesStructure import describes_tc_structure_part


def error(error_msg):
    print('Error:')
    print(error_msg)
    exit(1)


def main():
    try:
        separator = '\\' if '\\' in argv[1] else '/'  # path separator
        filename = argv[1].split(separator)[-1]
        if not filename.endswith('.ttcn'):
            error('Argument of the script has to be a .ttcn file')
    except IndexError:
        error('Argument containing name of .ttcn file is necessary for the script.')

    try:
        file = open(argv[1], 'r')
    except FileNotFoundError:
        error('File not found.')

    split_source = file.read().split('\n')
    file.close()

    lines_with_defined_testcases = [line_number for line_number, line in enumerate(split_source, 1) if
                                    line.strip().startswith('testcase')]
    print('Lines:')

    for i in range(len(lines_with_defined_testcases)):
        tc_line_number = lines_with_defined_testcases[i]
        try:
            next_tc_line_number = lines_with_defined_testcases[i + 1]  # line, where next test case is defined
        except IndexError:
            next_tc_line_number = len(split_source)

        # lines, where given parts are definied, -1 means that it has not been found in the file
        line_with_precondition = -1
        line_with_test_part = -1
        line_with_restore = -1
        line_with_check_before = -1
        line_with_check_after = -1

        for j, line in enumerate(split_source[tc_line_number:next_tc_line_number]):

            # searching for line numbers of parts that are necessary for the script
            if describes_tc_structure_part(line, 'PRECONDITION'):
                line_with_precondition = tc_line_number + j
            elif describes_tc_structure_part(line, 'TEST'):
                line_with_test_part = tc_line_number + j
            elif describes_tc_structure_part(line, 'RESTORE') or describes_tc_structure_part(line, 'RESTORATION'):
                line_with_restore = tc_line_number + j
            elif 'f_check_before' in line.lower():
                line_with_check_before = tc_line_number + j
            elif 'f_check_after' in line.lower():
                line_with_check_after = tc_line_number + j

        # checking if precondition, test and restore parts exist
        if line_with_precondition == -1:
            print(str(tc_line_number) + ' precondition part not found')
        if line_with_test_part == -1:
            print(str(tc_line_number) + ' test part not found')
        if line_with_restore == -1:
            print(str(tc_line_number) + ' restore part not found')

        # checking if function f_check_before exists and is performed in precondition part
        if line_with_check_before == -1:
            print(str(tc_line_number) + ' function f_check_before not found')
        elif not (0 < line_with_precondition < line_with_check_before < line_with_test_part):
            print(str(tc_line_number) + ' function f_check_before is not in precondition part')

        # checking if function f_check_after exists and is performed in restore part
        if line_with_check_after == -1:
            print(str(tc_line_number) + ' function f_check_after not found')
        elif line_with_restore > line_with_check_after:
            print(str(tc_line_number) + ' function f_check_after is not in restore part')


if __name__ == '__main__':
    main()
