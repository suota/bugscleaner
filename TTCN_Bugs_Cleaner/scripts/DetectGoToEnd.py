
# The script detects 'goto END' statements in the restore section
# of a TTCN test description, rev. PA1

from sys import argv
import re

restore = False
errors =[]

if not argv[1]:
    print("Error:")
    print("Input TD filename must be specified!")
    exit(1)

with open(argv[1]) as file:

    for i, line in enumerate(file,1):

        if re.match('\s*testcase\s+tc_', line, re.IGNORECASE):
            restore = False

        if re.match('^#{0}\s*label\s+END\s*;', line):
            restore = True

        if restore:
            if re.match('^#{0}.*goto\s+END\s*;', line):
                errors.append(i)

print("Lines:")
				
for elem in errors:
    print(str(elem) + " goto end after label END")


