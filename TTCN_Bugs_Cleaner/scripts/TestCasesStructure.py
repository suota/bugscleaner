
# This script checks if all testcases in the given .ttcn file have the following structure: precondition part,
# test part and restore part. Argument of the script is .ttcn file

from sys import argv


def describes_tc_structure_part(line, part_name):
    """
    Checks if given line describes test case's structure part. Possibilities:
    1) part name is in comment, e.g.    #   PRECONDITION
    2) part name is in log, e.g.        log("*********************PRECONDITION********************");
    :param line: line from the file
    :param part_name: name of tc's structure part
    :return: True if given line describes test case's structure part, False otherwise
    """
    return (line.lstrip().startswith('#') and part_name in line.split()) or ('log' in line and part_name in line)


def error(error_msg):
    print('Error:')
    print(error_msg)
    exit(1)


def main():
    try:
        separator = '\\' if '\\' in argv[1] else '/'  # path separator
        filename = argv[1].split(separator)[-1]
        if not filename.endswith('.ttcn'):
            error('Argument of the script has to be a .ttcn file')
    except IndexError:
        error('Argument containing name of .ttcn file is necessary for the script.')

    try:
        file = open(argv[1], 'r')
    except FileNotFoundError:
        error('File not found.')

    split_source = file.read().split('\n')
    file.close()

    lines_with_defined_testcases = [line_number for line_number, line in enumerate(split_source, 1) if
                                    line.strip().startswith('testcase')]
    print("Lines:")

    for i in range(len(lines_with_defined_testcases)):
        tc_line_number = lines_with_defined_testcases[i]
        try:
            next_tc_line_number = lines_with_defined_testcases[i + 1]  # line number, where next test case is defined
        except IndexError:
            next_tc_line_number = len(split_source)

        # lists which consist of line numbers where precondition/test/restore parts are defined
        lines_with_precondition = []
        lines_with_test = []
        lines_with_restore = []

        for j, line in enumerate(split_source[tc_line_number:next_tc_line_number]):

            # searching for line numbers where precondition/test/restore parts are defined
            if describes_tc_structure_part(line, 'PRECONDITION'):
                lines_with_precondition.append(tc_line_number + j)
            if describes_tc_structure_part(line, 'TEST'):
                lines_with_test.append(tc_line_number + j)
            if describes_tc_structure_part(line, 'RESTORE') or describes_tc_structure_part(line, 'RESTORATION'):
                lines_with_restore.append(tc_line_number + j)

        # every test case should have one precondition, one test and one restore part defined
        if len(lines_with_precondition) != 1:
            print(str(tc_line_number) + ' testcase contains ' + str(len(lines_with_precondition)) +
                  ' precondition parts')
        if len(lines_with_test) != 1:
            print(str(tc_line_number) + ' testcase contains ' + str(len(lines_with_test)) + ' test parts')
        if len(lines_with_restore) != 1:
            print(str(tc_line_number) + ' testcase contains ' + str(len(lines_with_restore)) + ' restore parts')

        # checking if all parts are defined in correct order (precondition, test, restore)
        try:
            if lines_with_precondition[0] > lines_with_test[0]:
                print(str(tc_line_number) + ' test part is before precondition part')
            if lines_with_test[0] > lines_with_restore[0]:
                print(str(tc_line_number) + ' restore part is before test part')
            if lines_with_precondition[0] > lines_with_restore[0]:
                print(str(tc_line_number) + ' restore part is before precondition part')
        except IndexError:
            pass


if __name__ == '__main__':
    main()
