#!/usr/bin/perl
#
# Check if the actions in the precondition section have
# appropriate counterparts in restore section of a TC, rev. PA1
#
# usage: perl ./ttcnrestore.pl in.ttcn

use strict;
use warnings;

if (@ARGV == 0)
{
    print "Error:\n";
    print "Input TD filename must be specified!\n";
    exit;
}

open (my $in, "<", $ARGV[0]) or die "Error:\nCannot open input TD file.";
 # update this hash table with more command pairs
my %commands = ("HESUI" => "HESUE",	"HEM2I" => "HECDM2E",	"HECDC2I" => "HECDMPE",
                "HECDMPI" => "HECDMPE",	"HEAPI" => "HEAPE",	"HEEQI" => "HEEQE",
                "HERCI" => "HERCE",	"HEGSI" => "HEGSE", 	"HEAMI" => "HEAME",
		"HECMI" => "HECME",	"HEISI" => "HEICE",	"HELDI" => "HELDE",
                "HECTI" => "HECTE",	"HECUI" => "HECUE",	"HEFSI" => "HEFSE",
                "HEGLI" => "HEGLE",	"HEGSI" => "HEGSE", 	"HEIXI" => "HEIXE",
                "HELDI" => "HELDE",	"HEMMI" => "HEMME",	"HEMII" => "HEMIE",
                "HEMRI" => "HEMRE",	"HENSI" => "HENSE",	"HEPVI" => "HEPVE",
                "HERDI" => "HERDE",	"HESFI" => "HESFE",	"HESGI" => "HESGE",
                "HESSI" => "HESSE",	"HESTI" => "HESTE",	"HEUPI" => "HEUPE",
                "HEXAI" => "HEXAE",	"HEZNI" => "HEZNE",	"HGSUI" => "HGSUE",
                "HGM2I" => "HGM2E",	"HGAMI" => "HGAME",	"HGAPI" => "HGAPE",
                "HGASI" => "HGASE",	"HGBDI" => "HGBDE",	"HGCMI" => "HGCME",
                "HGCTI" => "HGCTE",	"HGCUI" => "HGCUE",	"HGEQI" => "HGEQE",
                "HGFSI" => "HGFSE",	"HGGFI" => "HGGFE",	"HGGLI" => "HGGLE",
                "HGGSI" => "HGGSE",	"HGHSI" => "HGHSE",	"HGICI" => "HGICE",
                "HGIXI" => "HGIXE",	"HGLDI" => "HGLDE",	"HGMAI" => "HGMAE",
                "HGMCI" => "HGMCE",	"HGMFI" => "HGMFE",	"HGMHI" => "HGMHE",
                "HGMMI" => "HGMME",	"HGMRI" => "HGMRE",	"HGMSI" => "HGMSE",
                "HGNII" => "HGNIE",	"HGNSI" => "HGNSE",	"HGPAI" => "HGPAE",
                "HGPDI" => "HGPDE",	"HGPFI" => "HGPFE",	"HGPSI" => "HGPSE",
                "HGPVI" => "HGPVE",	"HGRCI" => "HGRCE",	"HGRDI" => "HGRDE",
                "HGRFI" => "HGRFE",	"HGSAI" => "HGSAE",	"HGSFI" => "HGSFE",
                "HGSGI" => "HGSGE",	"HGSII" => "HGSIE",	"HGSSI" => "HGSSE",
                "HGSTI" => "HGSTE",	"HGTEI" => "HGTEE",	"HGUPI" => "HGUPE",
                "HGXAI" => "HGXAE",	"HGZNI" => "HGZNE",
		"FGFII" => "FGFIE",	"FGFOI" => "FGFOE",	"FGGII" => "FGGIE",
		"FGNTI" => "FGNTE",	"FGONI" => "FGONE",	"FGSII" => "FGSIE",
		"FGGSI" => "FGGSE",	"FGIMI" => "FGIME",	"FGMLI" => "FGMLE",
		"FGSDI" => "FGSDE",	"FGSKI" => "FGSKE",	"FGSOI" => "FGSOE",
		"FGTTI" => "FGTTE",
		"NHBSI" => "NHBSE",	"NHGII" => "NHGIE",	"NHGSI" => "NHGSE",
		"NHNTI" => "NHNTE",	"NHTTI" => "NHTTE",	"AEBSI" => "AEBSE",
		"AGKDI" => "AGKDE",	"AGSUI" => "AGSUE",	"AGSII" => "AGSIE",
		"AGSOI" => "AGSOE"
);
my @command_init = (keys %commands);
my @actions = ();
my $tcid = 0;
my $comment = 0;                                 # test case title
my $counter = 0;

print "Lines:\n";
while (my $line = <$in>)
{
    $counter = $counter + 1;
	# Remove comments so warrnings will not be reported for them.
	if ($line =~ m/\/\//i && $comment == 0) {
		$line = substr($line, 0, index($line, "//"))."\n";
	}
	if ($line =~ m/\/\*/i && $line =~ m/\*\//i && $comment == 0) {
		if (rindex($line, "*/") - index($line, "/*") > 1) {
			$line = substr($line, 0, index($line, "/*"))." ".substr($line, 2 + rindex($line, "*/"));
		}
	}
	if ($line =~ m/\/\*/i && ($line !~ m/\*\//i || $line =~ m/\/\*\//i) && $comment == 0) {
		$comment = 1;
		$line = substr($line, 0, index($line, "/*"))."\n";
	}
	if ($line =~ m/\*\//i && $comment == 1) {
		$comment = 0;
		$line = substr($line, 2 + rindex($line, "*/"))."\n";
	}
	if ($comment == 1) {
		$line = "\n";
	}

	# Remove strings from log functions so string to log won't match in later part of script
	if ($line =~ m/log\s*\(/i && $line =~ m/\)/i) {
		while ($line =~ m/\"/i) {
			my $right = rindex(substr($line, 0, index($line, "\"") + 1), "\"");
			my $begin = substr($line, 0, $right);
			my $end   = substr($line, index($line, "\"") + 1, -1);
			$end = substr($end, index($end, "\"") + 1);
			$line = $begin."value".$end."\n";
		}
	}
	



    if ($line =~ m/^#{0}\s*testcase\s+tc_/i)    # start of a new testcase
    {

        if (scalar @actions > 0)
        {
            print "".$counter. " Missing command(s) [@actions] in RESTORE sector.\n";
        }

        @actions = ();                          # start with a clean list per each TC
        $tcid = $line;
        $tcid =~ s/(^#{0}\s*testcase\s+)(tc_\w*)(\s*\(\s*\)\s*runs\s*on\s*\w*\s*\{?\r*\n*)/$2/i;
        $tcid =~ s/_/ /g;                                  # save TC title
    }

    if ($line =~ m/^#{0}\s*control\s*\{*$/)     # reached past last TC
    {
        if (scalar @actions > 0)
        {
            print "".$counter. " Missing command(s) [@actions] in RESTORE sector.\n";
        }
    }

    foreach (@command_init)
    {
        if ($line =~ m/^\s*#{0}.*\"$_(?!.*t_FAULT_CODE.*)/i)
        {
            push @actions, $commands{$_};       # collect init commands used in a given TC        
        }
    }

    my $i = 0;
    foreach (@actions)
    {
        if ($line =~ m/^\s*#{0}.*\"$_(?!.*t_FAULT_CODE.*)/i)
        {
            splice @actions, $i, 1;             # discard each counterpart command found
        }
        $i++;
    }
}

close $in;
exit;
