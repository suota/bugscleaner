
# This script is used to detect lines of the code containing incorrectly named test cases and functions. It also checks
# validity of module's name. The argument of the script is .ttcn file in the format of: TDXXXX_slogan, where XXXX is
# feature's number and slogan is short description of the test cases. Every test case defined in this file has to have
# the same feature's number and slogan as the ones defined above.

from sys import argv
import re


def consists_of_digits(string):
    """ Returns true if given string consists of digits only. """
    return all(char.isdigit() for char in string)


def matches(string, pattern):
    """ Returns true if given string matches the regular expression included in pattern """
    result = pattern.match(string.upper())
    return False if result is None else True  # if result is None, it means that given string doesn't match the pattern


def check_name_correctness(line_number, name, feature_number, slogan, test_case=True):
    """
    Checks if test cases's / function's name is correct. Rules which have to be followed:
    - test case's name should start with 'tc_TD, while function's name should start with f_TD
    - test case's/function's feature number should be the same as the feature number from the filename
    - test case's/function's feature number should consist of digits only
    - test case's/function's slogan should be the same as the slogan from the filename
    :param line_number: line, where test case/function was defined, necessary to print warning message
    :param name: test case's/function's name which will be checked
    :param feature_number: filename's feature number
    :param slogan: filename's slogan
    :param test_case: True if variable name is the name of test case, otherwise name is function's name
    :return: True if test case's/function's name is correct, False otherwise
    """
    # let's assume that test case's name is tc_TDAAAA_001_tcSlogan
    starting_index = 5 if test_case else 4                                  # index, where feature number starts
    end_of_feature_index = starting_index + len(feature_number)             # index, where feature number ends
    tc_feature_number = name[starting_index:end_of_feature_index]           # returns 'AAAA'
    end_of_number_index = name.find('_', end_of_feature_index + 1)          # index, where tc number ends
    tc_number = name[(end_of_feature_index + 1):end_of_number_index]        # returns '001'
    tc_slogan = name[(end_of_number_index + 1):]                            # returns 'tcSlogan'

    # Warnings
    if test_case and not name.upper().startswith('TC_TD'):
        print(str(line_number) + ' Name of testcase doesn\'t start with tc_TD')
    elif not test_case and not name.upper().startswith('F_TD'):
        print(str(line_number) + ' Name of function doesn\'t start with f_TD')

    if tc_feature_number != feature_number:
        print(str(line_number) + ' Feature number of testcase is not the same as feature number in filename')

    if not consists_of_digits(tc_number):
        print(str(line_number) + ' Tc number doesn\'t consist of digits')

    if tc_slogan != slogan:
        print(str(line_number) + ' Slogan of testcase is not the same as slogan in filename')


def check_module_name(line_number, name, feature_number, slogan):
    """
    Checks if module name's is correct. Rules:
    - module's feature number should be the same as feature number defined in filename
    - module's slogan should be the same as slogan in filename
    :param line_number: line, where module was defined, necessary to print warning message
    :param name: name of the module
    :param feature_number: filename's feature number
    :param slogan: filename's slogan
    :return: True if module's name is correct, False otherwise
    """
    # let's assume that module's name is TDAAAA_tcSlogan
    end_of_feature_index = 2 + len(feature_number)        # index, where feature number ends
    module_feature_number = name[2:end_of_feature_index]  # returns 'AAAA'
    module_slogan = name[(end_of_feature_index + 1):]     # returns 'tcSlogan'

    # Warnings
    if module_feature_number != feature_number:
        print(str(line_number) + ' Module\'s feature number is not the same as feature number in filename')

    if module_slogan != slogan:
        print(str(line_number) + ' Module\'s slogan is not the same as slogan in filename')


def error(error_msg):
    print('Error:')
    print(error_msg)
    exit(1)


def main():
    # file name should have following form: TD[one or more digits]_[zero or more digits,letters, (, ) or _].ttcn
    pattern = re.compile(r'TD([0-9])+_[A-Z0-9()_]*')  # extension is checked later
    try:
        separator = '\\' if '\\' in argv[1] else '/'  # path separator
        filename = argv[1].split(separator)[-1]
        if matches(filename, pattern) and filename.endswith('.ttcn'):
            # let's assume that the name of the file is TDXXXX_some_slogan.ttcn
            split_filename = filename[:-5].split('_')  # returns ['TDXXXX', 'some', 'slogan']
            feature_number = split_filename[0][2:]    # returns 'XXXX' - feature number
            slogan = '_'.join(split_filename[1:])     # returns 'some_slogan'
        else:
            error('Argument of the script has to be a file of format TDXXXX_tcSlogan.ttcn')
    except IndexError:
        error('Argument containing name of .ttcn file is necessary for the script.')

    try:
        file = open(argv[1], 'r')
    except FileNotFoundError:
        error('File not found.')

    split_source = file.read().split('\n')
    file.close()

    # Creating lists of tuples, where first element is line number (counting from 1), and second is the name of test
    # case or a function.
    tc_names = [(i, line.strip().split(' ')[1]) for i, line in enumerate(split_source, 1)
                if line.strip().startswith('testcase')]  # example: testcase tc_TD0000 in 10th line -> (10, tc_TD0000)
    function_names = [(i, line.strip().split(' ')[1]) for i, line in enumerate(split_source, 1)
                      if line.strip().startswith('function')]
    line_with_module_name, module_name = next((i, line.strip().split(' ')[1]) for i, line in enumerate(split_source, 1)
                                              if line.strip().startswith('module'))  # only one module in the file

    print("Lines:")
    check_module_name(line_with_module_name, module_name, feature_number, slogan)

    test_cases_number = len(tc_names)
    for i in range(test_cases_number):

        line_number, tc_name = tc_names[i]
        check_name_correctness(line_number, tc_name.replace('()', ''), feature_number, slogan)

        line_number, function_name = function_names[i]
        check_name_correctness(line_number, function_name.replace('()', ''), feature_number, slogan, False)


if __name__ == '__main__':
    main()
