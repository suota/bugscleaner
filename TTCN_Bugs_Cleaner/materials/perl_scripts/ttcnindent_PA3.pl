#!/usr/bin/perl
#
# This script modifies indentation by counting curly {} and round () brackets 
#
# PA1   - 21.10.2013 - XDAWBOI
# PA2   - 16.06.2014 - XDZMVAS
# PA3   - 05.01.2015 - XDAWBOI
#
# Usage:
# perl ./ttcnindent.pl input.ttcn output.ttcn

use strict;
use warnings;
use POSIX;

my $output = "output.ttcn";		# Default input file
my $indent = "  ";				# Indent characters - change here if neccesary
								# Dont use tabulator '\t' as an indent - it is considered as a warrining in mctr_gui framework.

# Function to remove white spaces from begining & ending of input string
sub trim($) {
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

# Validate input parameters and prepare output path
if (@ARGV == 0 || @ARGV == 1 && !($ARGV[0] eq "-man" || $ARGV[0] eq "--man" || $ARGV[0] eq "-help" || $ARGV[0] eq "--help")) {
    print "Input TD & output filenames must be specified!\n";
    exit;
} elsif (@ARGV == 1 && ($ARGV[0] eq "-man" || $ARGV[0] eq "--man" || $ARGV[0] eq "-help" || $ARGV[0] eq "--help")) {
	print "Usage:\n ./ttcnindent_PA3.pl input.file output.file\n\n";
	print "input.file  - input TD, writen in TTCN,\n";
	print "output.file - file to be created by script & containing TD with\n";
	print "              correct indentation.\n\n";
	exit;
} elsif (@ARGV == 2) {
	# Input & output files cannot be the same file
	if ($ARGV[0] eq $ARGV[1]) {
		print "Input TD & output filenames must be different files!\n";
    	exit;
	}

	# Get absolute Windows' path to input file
	if ($ARGV[0] =~ m/\\/i) {
		$output = substr($ARGV[0], 0, rindex($ARGV[0], "\\") + 1)."_output.ttcn";
	}
	
	# Get absolute Unix' path to input file
	elsif ($ARGV[0] =~ m/\//i) {
		$output = substr($ARGV[0], 0, rindex($ARGV[0], "\/") + 1)."_output.ttcn";
	
	# Input file has no absolute path
	} else {
		$output = $ARGV[1];
	}
} else {
	print "Too many input arguments!\n";
    exit;
}

# Open input & output files
open (my $in, "<", $ARGV[0]) or die "Error opening input TD file";
open(DESTFILE, '>:unix', $output);

my $i = 0;					# line number
my $cnt = 0;				# curly brackets counter
my $comment = 0;			# comment indicator for removing comments code

my $countRoundBracket = 0;
my $countRoundBracketLine = 0;
my $lastRoundBracketPos = 0;

# Parse line by line input file, and at the end put parsed line to output file
while (my $line = <$in>) {
	$i++;
	my $mod_line = trim($line);
	
	# Remove comments so they won't produce abnormalities in indentation
	if ($line =~ m/\/\//i && $comment == 0) {
		$line = substr($line, 0, index($line, "//"))."\n";
	}
	if ($line =~ m/\/\*/i && $line =~ m/\*\//i && $comment == 0) {
		if (rindex($line, "*/") - index($line, "/*") > 1) {
			$line = substr($line, 0, index($line, "/*"))." ".substr($line, 2 + rindex($line, "*/"));
		}
	}
	if ($line =~ m/\/\*/i && ($line !~ m/\*\//i || $line =~ m/\/\*\//i) && $comment == 0) {
		$comment = 1;
		$line = substr($line, 0, index($line, "/*"))."\n";
	}
	if ($line =~ m/\*\//i && $comment == 1) {
		$comment = 0;
		$line = substr($line, 2 + rindex($line, "*/"))."\n";
	}
	if ($comment == 1) {
		$line = "\n";
	}
	
	# Count indent lvl (curly brackets)
	$cnt += $line =~ tr/\{// - $line =~ tr/\}//;
	$countRoundBracketLine = $line =~ tr/\(// - $line =~ tr/\)//; 
	$countRoundBracket += $countRoundBracketLine;
	if ($countRoundBracketLine > 0){
		if ($line =~ '.*?\([\s]*([\w\{]+)'){
			$lastRoundBracketPos = index($mod_line, $1);
		} else {
			$lastRoundBracketPos = rindex($mod_line, "(") + 1;
		}
	}
	
	# Insert indentation to a modified line (modified only by trim function)
	# 2nd IF prevents the lonely curly bracket "{" from beeing to deep
	if ($comment == 0 && $mod_line !~ m/\*\//i) {
		if ($line =~ m/\{/i && $line !~ m/\}/i) {
			for my $j (0 .. $cnt - 2) {
				$mod_line = $indent.$mod_line;
			}
		} elsif ($line !~ m/\(/i && ($countRoundBracket > 0 || $countRoundBracketLine < 0 )){
			for my $j (0 .. ceil($lastRoundBracketPos/2)) {
				$mod_line = $indent.$mod_line;
			}
			if ($lastRoundBracketPos % 2){
				$mod_line = " ".$mod_line;
			} else {
				$mod_line = $indent.$mod_line;
			}
		} else  {
			for my $j (0 .. $cnt - 1) {
				$mod_line = $indent.$mod_line;
			}
		}

	}
	
	print DESTFILE $mod_line."\n";
}

if ($cnt != 0) {
	print "Something gone wrong with curly blocks since counter hasn't gone down to 0!\nCount: ".$cnt;
} else {
	print "Script ended with success! Result is in file:\n".$output."\n";
}

close DESTFILE;
close $in;
exit;
	