#!/usr/bin/perl
#
# This script modifies indentation by counting curly brackets {}
#
# PA1   - 10.06.2014 - XDZMVAS
#
# Usage:
# perl ./ttcntcparts.pl input.ttcn
#
# Speciality in syntax of processed file:
#
# Testcase and function must be in pair, and testcase must be first
# Strings that concern to verifying parts must be : PRECONDITION, TEST, RESTORE.
# Template for strings looks like:
# /*************************************************************#
# # -------------------------------------------------------------
# # PRECONDITION
# # -------------------------------------------------------------
# #*************************************************************/
#


use strict;
use warnings;

#parse parameters
if (scalar @ARGV == 0)
{
    print "No filename have been found\n";
    exit;
}
else
{
    open(PROCESSEDFILE, $ARGV[0]) or die $!;

    print "\n################################################\n";
    print " File : ".$ARGV[0]."\n";
    print "################################################\n";

    my $count = 0;
    my $isAllCorrect = 1;
    my $testCaseName = "";
    my $commentBorderStart = 0;
    my $commentBorderEnd = 0;
    my $checked = 0;
    my $precondition = 0;
    my $test = 0;
    my $restore = 0;
    my $functionStart = 0;
    my $multiLineComment = 0;
    my $commentsBorderProblem = 0;
    my $commentsBorderProblemStr = "";
   
    LINE: while(<PROCESSEDFILE>)
    {
	#Search PRECONDITION, TEST and RESTORE in multiline comment's block
	#other lines in multiline comment are skiped
    	if ($multiLineComment == 1)
    	{
                if ($commentBorderEnd != 1)
		{
		    #Search border line 
		    if ($_=~ '^#[\s]*\-+[\s]*$')
		    {
			if ($commentBorderStart == 1 and $checked > 0)
			{
			    $commentBorderEnd = 1;
			    $commentBorderStart = 0;
			    next LINE;
			}
			elsif ($commentBorderStart == 0 and $checked > 0)
			{
			    $commentsBorderProblem = 1;
			    $commentBorderEnd = 1;
			    if ($checked == 1)
			    {
				$commentsBorderProblemStr .= " Precondition";
			    }
			    elsif ($checked == 2)
			    {
				$commentsBorderProblemStr .= " Test";
			    }
			    elsif ($checked == 3)
			    {
				$commentsBorderProblemStr .= " Restore";
			    }
			    next LINE;
			}
			$commentBorderStart = 1; #($commentBorderStart == 0 ? 1:0);
			next LINE;
		    }
		#Compare text into between border lines
# 		if ($commentStart == 1)
# 		{
		    if ($_=~ '^#[\s]*([\w]+)[\s]*$')
		    {
			if ($1 eq "PRECONDITION")
			{
			    $precondition = 1;
			    $checked = 1;
			}
			elsif ($1 eq "TEST")
			{
			    $test = 1;
			    $checked = 2;
			}
			elsif ($1 eq "RESTORE")
			{
			    $restore = 1;
			    $checked = 3;
			}
			next LINE;
		    }
		}
		#End of multiline comment
		next LINE if $_ !~ '^(.*)(\*/)[\s]*$';
		if ($commentBorderEnd != 1 and $checked > 0 and $commentBorderStart != 0)
		{
		   if ($checked == 1)
		   {
		      $commentsBorderProblemStr .= " Precondition";
		   }
		   elsif ($checked == 2)
		   {
		      $commentsBorderProblemStr .= " Test";
		   }
		   elsif ($checked == 3)
		   {
		      $commentsBorderProblemStr .= " Restore";
		   }
		}
		$commentBorderStart = 0;
		$commentBorderEnd = 0;
		$multiLineComment = 0;
		$checked = 0;
		next LINE
	}

	if ($testCaseName ne "")
	{
	    #Search start of function
	    if ($_ =~ '^[\s]*function[\s]+([\w\d]+)[\s]*(\(.*\)+?)(.*?)(\{?)[\s]*$')
	    {
		$functionStart = 1;
		if ($4 eq "{")
		{
		    $count = $count + 1;
		}
		next LINE;
	    }
	    #Search brackets and multiline comments into function's body
	    if ($functionStart == 1)
	    {
		#Comment in one line
		next LINE if $_ =~ '^[\s]*[/]{2}(.*)[\s]*$';
		#MultiLineComment in one line
		next LINE if $_ =~ '^[\s]*\/\*(.*)\*\/[\s]*$';
		#MultiLineComment in multi line
		if ($_ =~ '^[\s]*\/\*(.*)(?!\*\/)[\s]*$')
		{
		      $multiLineComment = 1;
		      next LINE
		}
		#Count of brackets
		$count += $_ =~ tr/{//;
		$count -= $_ =~ tr/}//;
		#End of testcase
		if ($count == 0)
		{
		    if (!(($precondition == 1) and ($test == 1) and ($restore == 1) and ($commentsBorderProblem == 0)))
		    {
			print "\n";
			print "Test Case: ".$testCaseName."\n";
			if ($commentsBorderProblem != 0)
			{
			  print "Problem with border line: ".$commentsBorderProblemStr;
			  print "\n";
			}
			
                        if (!(($precondition == 1) and ($test == 1) and ($restore == 1)))
			{
			  print "Expected:";
			  if ($precondition != 1)
			  {
			    print " Precondition";
			  }
			  if ($test != 1)
			  {
			    print " Test";
			  }
			  if ($restore != 1)
			  {
			    print " Restore";
			  }
			  print "\n";
			}
			$isAllCorrect = 0;
		    }
		    $testCaseName = "";
		    $precondition = 0;
		    $test = 0;
		    $restore = 0;
		    $commentBorderStart = 0;
		    $commentBorderEnd = 0;
		    $functionStart = 0;
		    $commentsBorderProblem = 0;
		    $commentsBorderProblemStr = "";
		}
	    }
	}

	#Start of testcase
        if ($_ =~ '^[\s]*testcase[\s]+([\w\d]+)[\s]*\(.*\)?([\s\d\w]+\{)?[\s]*$')
	{
	    $testCaseName = $1;
	    next LINE;
	}
    }
    #In case when all testcases have all parts
    if ($isAllCorrect == 1)
    {
	print "All TC are correct.\n"
    }

    print "\n";
    close PROCESSEDFILE;  
}