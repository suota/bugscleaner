#!/usr/bin/perl
#
# This script checks if every test case uses ALT instruction,
# so it won't get stuck infinitly during abnormal execution.
#
# PA2   - 21.10.2013 - XDAWBOI
#
# Usage:
# perl ./ttcnalt.pl input.ttcn

use strict;
use warnings;

# Function to remove white spaces from begining & ending of input string
sub trim($) {
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

if (@ARGV == 0) {
    print "Input TD filename must be specified!\n";
    exit;
}
else
{
    print "\n****************************************** \n";
    print "Checking alt-step initiation in testcases\n";
    print "****************************************** \n\n";
}

open (my $in, "<", $ARGV[0]) or die "Error opening input TD file";

my $i = 0;                      	# line number
my $warrning_count = 0;			# counts warrings while printing
my $comment = 0;			# comment indicator for removing comments code
my $cnt = 0;				# curly brackets counter
my $tc_line = 0;                	# line number to report unsafe testcase
my $indicator_tc = 0;			# indicator of testcase end (uses $cnt)
my $tc_body = "";			# testcase body in form of string
my @tc_name;				# testcase name in form of array strings
		
while (my $line = <$in>) {
	$i++;
	
	# Remove comments so warrnings will not be reported for them.
	if ($line =~ m/\/\//i && $comment == 0) {
		$line = substr($line, 0, index($line, "//"))."\n";
	}
	if ($line =~ m/\/\*/i && $line =~ m/\*\//i && $comment == 0) {
		if (rindex($line, "*/") - index($line, "/*") > 1) {
			$line = substr($line, 0, index($line, "/*"))." ".substr($line, 2 + rindex($line, "*/"));
		}
	}
	if ($line =~ m/\/\*/i && ($line !~ m/\*\//i || $line =~ m/\/\*\//i) && $comment == 0) {
		$comment = 1;
		$line = substr($line, 0, index($line, "/*"))."\n";
	}
	if ($line =~ m/\*\//i && $comment == 1) {
		$comment = 0;
		$line = substr($line, 2 + rindex($line, "*/"))."\n";
	}
	if ($comment == 1) {
		$line = "\n";
	}
	
	# Remove strings from log functions so string to log won't match in later part of script
	if ($line =~ m/log\s*\(/i && $line =~ m/\)/i) {
		while ($line =~ m/\"/i) {
			my $right = rindex(substr($line, 0, index($line, "\"") + 1), "\"");
			my $begin = substr($line, 0, $right);
			my $end   = substr($line, index($line, "\"") + 1, -1);
			$end = substr($end, index($end, "\"") + 1);
			$line = $begin."value".$end."\n";
		}
	}
	elsif ($line =~ m/log\s*\(/i) {
		print ++$warrning_count." : Abnormal log funciton in line ".$i.". (log function is not in 1-line or maybe msg is weird)\n";
	}
	
	# Check if new testcase has started and set indicator for testcase end detection
	# and get testcase start line for report purposes
	if ($line =~ m/\s*testcase\s+tc_/i && $indicator_tc == 0) {
		$indicator_tc = $cnt;
		$tc_line = $i;
		if ($line =~ m/^#{0}\s*testcase\s+(tc_\w+)/i)
		{
			#print "line $i: testcase: $1\n";
			@tc_name = split('_',$1);
		}
	}
	
	# Update curly {} brackets count.
	$cnt += $line =~ tr/\{// - $line =~ tr/\}//;
	
	# Acumulate testcase body (without empty lines for optimization)
	if ($indicator_tc != 0 && length(trim($line)) != 0) {
		$tc_body .= $line;
	}
	# Start processing testcase body
	if ($indicator_tc == $cnt && $line !~ m/testcase\s+tc_/i && $indicator_tc != 0) {
		
		# Mark testcase end
		$indicator_tc = 0;
		
		my $indicator_timer = 0;			# indicator of timer deficition presence
		my $indicator_alt = 0;				# indicator of alt-step presence
		my $indicator_timeout = 0;			# indicator of timeout-step presence
		my $indicator_map = 0;				# indicator of map/unmap presence
		
		# Parse testcase body line by line and search for safe-alt-step presence
		my @splited = split('\n', $tc_body);
		foreach my $line (@splited) {
			
			# Look for timer definition
			if ($indicator_timer == 0 && $indicator_alt == 0 && $indicator_timeout == 0 && $indicator_map == 0) {
				$indicator_timer = 1 if ($line =~ m/timer\s+t_/i);
			}
			
			# Look for alt presence after timer is defined
			if ($indicator_timer == 1 && $indicator_alt == 0 && $indicator_timeout == 0 && $indicator_map == 0) {
				$indicator_alt = 1 if ($line =~ m/alt/i);
			}
			
			# Look for timeout presence after timer and alt step are already found
			if ($indicator_timer == 1 && $indicator_alt == 1 && $indicator_timeout == 0 && $indicator_map == 0) {
				$indicator_timeout = 1 if ($line =~ m/\.timeout/i);
			}
			
			# Look for map/unmap presence (unwelcome)
			$indicator_map = 1 if ($line =~ m/map\s*\(/i);
		}
		
		# Check if timer & alt & timeout are presence and lack of map/unmap
		if (!($indicator_timer == 1 && $indicator_alt == 1 && $indicator_timeout == 1 && $indicator_map == 0)) {
			print ++$warrning_count." :\t Testcase number " . $tc_name[2] . " in line " .$tc_line. " does not use safe alt-step initiation.\n";
		}
		$tc_body = "";
	}

}

print "\nScript ended with success!\n";

close $in;
exit;
