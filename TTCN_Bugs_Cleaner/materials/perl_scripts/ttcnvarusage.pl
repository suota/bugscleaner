#!/usr/bin/perl
#
# Thi sscript checks the usage of defined variable and reports a warrning
# if a variable is not used.
#
# This script does not support nested testcases/functions. It supports
# only testcases/functions within main component (1st lvl of curly
# brackets {} ).
# 
# PA2   - 21.10.2013 - XDAWBOI
#
# Usage:
# perl ./ttcnnaming.pl input.ttcn

use strict;
use warnings;

# Function to remove white spaces from begining & ending of input string
sub trim($) {
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

if (@ARGV == 0) {
    print "Input TD filename must be specified!\n";
    exit;
}
else
{   print "\n**************************\n";
    print "Detecting unused variables\n";
    print "**************************\n\n";
}

open (my $in, "<", $ARGV[0]) or die "$ARGV[0] <- ",$!;

my $i = 0;					# line nr
my $warrning_count = 0;		# counts warrings while printing
my $comment = 0;			# comment indicator for removing comments code
my $cnt = 0;				# curly brackets counter
my $fun_line = 0;			# line nr to report purposes
my $indicator_fun = 0;		# indicator of function end (uses $cnt)
my $fun_body = "";			# function body in form of string
my $tc_line = 0;			# line nr to report purposes
my $indicator_tc = 0;		# indicator of testcase end (uses $cnt)
my $tc_body = "";			# testcase body in form of string
my $tc_name = "";			# testcase name in form of string
my @tc_num = ();
my @f_num = ();

while (my $line = <$in>) {
	$i++;
	
	# Remove comments so warrnings will not be reported for them.
	if ($line =~ m/\/\//i && $comment == 0) {
		$line = substr($line, 0, index($line, "//"))."\n";
	}
	if ($line =~ m/\/\*/i && $line =~ m/\*\//i && $comment == 0) {
		if (rindex($line, "*/") - index($line, "/*") > 1) {
			$line = substr($line, 0, index($line, "/*"))." ".substr($line, 2 + rindex($line, "*/"));
		}
	}
	if ($line =~ m/\/\*/i && ($line !~ m/\*\//i || $line =~ m/\/\*\//i) && $comment == 0) {
		$comment = 1;
		$line = substr($line, 0, index($line, "/*"))."\n";
	}
	if ($line =~ m/\*\//i && $comment == 1) {
		$comment = 0;
		$line = substr($line, 2 + rindex($line, "*/"))."\n";
	}
	if ($comment == 1) {
		$line = "\n";
	}
	
	# Remove strings from log functions so string to log won't match in later part of script
	if ($line =~ m/log\s*\(/i && $line =~ m/\)/i) {
		while ($line =~ m/\"/i) {
			my $right = rindex(substr($line, 0, index($line, "\"") + 1), "\"");
			my $begin = substr($line, 0, $right);
			my $end   = substr($line, index($line, "\"") + 1, -1);
			$end = substr($end, index($end, "\"") + 1);
			$line = $begin."value".$end."\n";
		}
	}
	elsif ($line =~ m/log\s*\(/i) {
		print ++$warrning_count." : Abnormal log funciton in line ".$i.". (log function is not in 1-line or maybe msg is weird)\n";
	}
	
	# Check if new testcase has started and set indicator for testcase end detection
	# and get testcase start line for report purposes
	if ($line =~ m/\s*testcase\s+(tc_\w+)/i && $indicator_tc == 0) {
		$indicator_tc = $cnt;
		$tc_line = $i;
		$tc_name = $1;
		@tc_num = split ('_',$1);
	}
	
	# Check if new function has started and set indicator for function end detection
	# and get function start line for report purposes
	if ($line =~ m/\s*function\s+(f_\w+)/i && $indicator_fun == 0) {
		$indicator_fun = $cnt;
		$fun_line = $i;
		@f_num = split ('_',$1);
	}
	
	# Update curly {} brackets count.
	$cnt += $line =~ tr/\{// - $line =~ tr/\}//;
	
	# Acumulate testcase body (without empty lines for optimization)
	if ($indicator_tc != 0 && length(trim($line)) != 0) {
		$tc_body .= $line;
	}
	
	# Acumulate function body (without empty lines for optimization)
	if ($indicator_fun != 0 && length(trim($line)) != 0) {
		$fun_body .= $line;
	}
	
	# Start processing testcase body
	if ($indicator_tc == $cnt && $line !~ m/testcase\s+tc_/i && $indicator_tc != 0) {
		
		# Mark testcase end
		$indicator_tc = 0;
		
		# Splite by new line character and retrieve all var names
		my @splited = split('\n', $tc_body);
		my $var_total = "";
		my $var_indicator = 0;
		
		my @vars;
		
		# First loop - retrieve var name by processing line by line
		foreach my $line (@splited) {
			
			# Accumulate multiline declatarions
			if ($line =~ m/^\s*var\s+/i || $var_indicator == 1) {
				$var_indicator = 1;
				$var_total .= " ".trim($line);
				
				# Detects end of variables declarations
				if ($line =~ m/;\s*/i) {
					$var_total = trim($var_total)."\n";
					$var_indicator = 0;
					
					# Eliminate table [] value initiation (eliminate extra colons for later processing)
					if ($var_total =~ m/\[[0-9]+\]\s*:=\s*\{/i && $var_total =~ m/\}/i) {
						$var_total = substr($var_total, 0, index($var_total, '{')) . "value" . substr($var_total, 1 + rindex($var_total, '}'));
					}
					
					# Eliminate table () value initiation (eliminate extra colons for later processing)
					if ($var_total =~ m/\(/i && $var_total =~ m/\)/i) {
						while ($var_total =~ m/\(/i && $var_total =~ m/\)/i) {
							my $stop  = rindex(substr($var_total, 0, index($var_total, ")") + 1), "(");
							my $begin = substr($var_total, 0, $stop);
							my $end = substr($var_total, index($var_total, ")") + 1, -1);
							$var_total = $begin."value".$end."\n";
						}
					}
					
					# Eliminate table {} value initiation (eliminate extra colons for later processing)
					if ($var_total =~ m/\s*:=\s*\{/i && $var_total =~ m/\}\s*;/i) {
						while ($var_total =~ m/\{/i && $var_total =~ m/\}/i) {
							my $stop  = rindex(substr($var_total, 0, index($var_total, "}") + 1), "{");
							my $begin = substr($var_total, 0, $stop);
							my $end = substr($var_total, index($var_total, "}") + 1, -1);
							$var_total = $begin."value".$end."\n";
						}
					}
					
					# Splite variables by colon and for each extract its own name (only name)
					my @splited2nd = split(/,+/, $var_total);
					
					# Insert white space " " before value initiation := (without it script omits variable)
					foreach my $var (@splited2nd) {
						if ($var =~ m/[a-z0-9]:=/i) {
							$var = trim(substr($var, 0, index($var, ":="))." ".substr($var, index($var, ":=")));
						}
					}
					
					# Extract variable name
					for my $j (0 .. $#splited2nd) {
						
						# Splite by white space (this is the reson for insertion of " " before :=) and extract variable name (only name)
						my @splited3rd = split(/\s+/, $splited2nd[$j]);
						for my $k (0 .. $#splited3rd) {
							
							# Extract name from "var [not template] type name [:= value]"
							if ($k <= scalar @splited3rd - 2 && $splited3rd[$k] =~ m/^var\s*/i && $splited3rd[$k + 1] !~ m/^template\s*/i) {
								$splited2nd[$j] = $splited3rd[$k + 2];
								last;
							}
							
							# Extract name from "var template type name [:= value]"
							if ($k <= scalar @splited3rd - 3 && $splited3rd[$k] =~ m/^var\s*/i && $splited3rd[$k + 1] =~ m/^template\s*/i) {
								$splited2nd[$j] = $splited3rd[$k + 3];
								last;
							}
							
							# Extract name from "[not var] name := value"
							if ($k < scalar @splited3rd - 1 && $splited3rd[$k + 1] =~ m/\s*:=\s*/i) {
								$splited2nd[$j] = $splited3rd[$k];
								last;
							}
							
							# Other cases are left unchanged, usless characters must be removed from those cases (see below)
						}
					}
					
					# Get rid of usless characters (white spaces, semicolon and value initiation := )
					foreach my $var (@splited2nd) {
						$var = trim($var);
						if ($var =~ m/;$/i) { $var = substr($var, 0, -1); }
						if ($var =~ m/:=$/i) { $var = substr($var, 0, -2); }
						$var = trim($var);
						
						# Put clean variable name in array for used/unused processing
						push(@vars, $var);
					}
					$var_total = "";
				}
			}
		}
		
		# Second loop to count variable appearance
		for my $var (@vars) {
			my $regex = quotemeta $var;
			my $count = () = $tc_body =~ /\b$regex\b/g;
			if ($count <= 1) {
				print ++$warrning_count.": \tIn testcase number ". $tc_num[2] ." in line ".$tc_line.":\t".$var."\n";
			}
		}
		
		$tc_body = "";
	}
	
	# Start processing function body
	if ($indicator_fun == $cnt && $line !~ m/function\s+f_/i && $indicator_fun != 0) {
		
		# Mark function end
		$indicator_fun = 0;
		
		# Splite by new line character and retrieve all var names
		my @splited = split('\n', $fun_body);
		my $var_total = "";
		my $var_indicator = 0;
		
		my @vars;
		
		# First loop - retrieve var name by processing line by line
		foreach my $line (@splited) {
			
			# Accumulate multiline declatarions
			if ($line =~ m/^\s*var\s+/i || $var_indicator == 1) {
				$var_indicator = 1;
				$var_total .= " ".trim($line);
				
				# Detects end of variables declarations
				if ($line =~ m/;\s*/i) {
					$var_total = trim($var_total)."\n";
					$var_indicator = 0;
					
					# Eliminate table [] value initiation (eliminate extra colons for later processing)
					if ($var_total =~ m/\[[0-9]+\]\s*:=\s*\{/i && $var_total =~ m/\}/i) {
						$var_total = substr($var_total, 0, index($var_total, '{')) . "value" . substr($var_total, 1 + rindex($var_total, '}'));
					}
					
					# Eliminate table () value initiation (eliminate extra colons for later processing)
					if ($var_total =~ m/\(/i && $var_total =~ m/\)/i) {
						while ($var_total =~ m/\(/i && $var_total =~ m/\)/i) {
							my $stop  = rindex(substr($var_total, 0, index($var_total, ")") + 1), "(");
							my $begin = substr($var_total, 0, $stop);
							my $end = substr($var_total, index($var_total, ")") + 1, -1);
							$var_total = $begin."value".$end."\n";
						}
					}
					
					# Eliminate table {} value initiation (eliminate extra colons for later processing)
					if ($var_total =~ m/\s*:=\s*\{/i && $var_total =~ m/\}\s*;/i) {
						while ($var_total =~ m/\{/i && $var_total =~ m/\}/i) {
							my $stop  = rindex(substr($var_total, 0, index($var_total, "}") + 1), "{");
							my $begin = substr($var_total, 0, $stop);
							my $end = substr($var_total, index($var_total, "}") + 1, -1);
							$var_total = $begin."value".$end."\n";
						}
					}
					
					# Splite variables by colon and for each extract its own name (only name)
					my @splited2nd = split(/,+/, $var_total);
					
					# Insert white space " " before value initiation := (without it script omits variable)
					foreach my $var (@splited2nd) {
						if ($var =~ m/[a-z0-9]:=/i) {
							$var = trim(substr($var, 0, index($var, ":="))." ".substr($var, index($var, ":=")));
						}
					}
					
					# Extract variable name
					for my $j (0 .. $#splited2nd) {
						
						# Splite by white space (this is the reson for insertion of " " before :=) and extract variable name (only name)
						my @splited3rd = split(/\s+/, $splited2nd[$j]);
						for my $k (0 .. $#splited3rd) {
							
							# Extract name from "var [not template] type name [:= value]"
							if ($k <= scalar @splited3rd - 2 && $splited3rd[$k] =~ m/^var\s*/i && $splited3rd[$k + 1] !~ m/^template\s*/i) {
								$splited2nd[$j] = $splited3rd[$k + 2];
								last;
							}
							
							# Extract name from "var template type name [:= value]"
							if ($k <= scalar @splited3rd - 3 && $splited3rd[$k] =~ m/^var\s*/i && $splited3rd[$k + 1] =~ m/^template\s*/i) {
								$splited2nd[$j] = $splited3rd[$k + 3];
								last;
							}
							
							# Extract name from "[not var] name := value"
							if ($k < scalar @splited3rd - 1 && $splited3rd[$k + 1] =~ m/\s*:=\s*/i) {
								$splited2nd[$j] = $splited3rd[$k];
								last;
							}
							
							# Other cases are left unchanged, usless characters must be removed from those cases (see below)
						}
					}
					
					# Get rid of usless characters (white spaces, semicolon and value initiation := )
					foreach my $var (@splited2nd) {
						$var = trim($var);
						if ($var =~ m/;$/i) { $var = substr($var, 0, -1); }
						if ($var =~ m/:=$/i) { $var = substr($var, 0, -2); }
						$var = trim($var);
						
						# Put clean variable name in array for used/unused processing
						push(@vars, $var);
					}
					$var_total = "";
				}
			}
		}
		
		# Second loop to count variable appearance
		for my $var (@vars) {
			my $regex = quotemeta $var;
			my $count = () = $fun_body =~ /\b$regex\b/g;
			if ($count <= 1) {
				print ++$warrning_count.": \tIn function number ". $f_num[2] ." in line ".$fun_line.":\t".$var."\n";
			}
		}
		
		$fun_body = "";
	}
	
}

if ($cnt != 0) {
	print "\nSomething gone wrong with curly blocks since counter hasn't gone down to 0!\n";
} else {
	print "\nScript ended with success!\n\n";
}

close $in;
exit;
