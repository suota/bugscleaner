#!/usr/bin/perl
#
# The script detects 'goto END' statements in the restore section
# of a TTCN test description, rev. PA1
#
# usage: ./ttcndetectgotoend.pl in.ttcn

use strict;
use warnings;
my @errors;
my $errors_count = 0;

if (@ARGV == 0)
{
    print "Input TD filename must be specified!\n";
    exit;
}
else {

    print "\n***************************************** \n";
    print "Detecting 'goto END' after 'label END'.\n";
    print "***************************************** \n";
}

open (my $in, "<", $ARGV[0]) or die "$ARGV[0] <- ",$!;

my $i = 0;                      # line number
my $restore = 0;                # are we in the RESTORE section

while (my $line = <$in>)
{
    $i++;
    $restore = 0 if ($line =~ m/^#{0}\s*testcase\s+tc_/i);      # start of a new testcase
    $restore = 1 if ($line =~ m/^#{0}\s*label\s+END\s*;/);      # start of RESTORE section detected

    if ($restore == 1)
    {
         push @errors,$i if ($line =~ m/^#{0}.*goto\s+END\s*;/);
         #print "\tMindless usage of 'goto END' at line ".$i.". Remove it!\n"
    }
}


my $error_count = $#errors + 1;
if ($error_count > 0)
{
	print "\tMindless usage of 'goto END' detected!\n";
	if ($error_count == 1)
	{
		print "\tRemove it from line: \n\n";
	}
	else
	{
		print "\tRemove it from following lines: \n\n\t";
	}
	foreach (@errors)
	{
		$errors_count++;
		if ($errors_count % 4 == 0)
		{
			print "$_\n\t";
		}
		else
		{
			print "$_\t";
		}
	}
        print "\n\n";
}


close $in;
exit;
