package graphics;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;

import handlers.CoreSupport;
import net.miginfocom.swt.MigLayout;

public class AboutTab {
	Composite mainComposite;
	Image logo;
	Label logoLabel;
	String logoPath;
	StyledText content;
	StyleRange[] ranges;
	Listener scrollBarListener;
	Color black;
	Color white;
	String text = 	  "Main:\n"
					+ "		Listed scripts: there are listed all python and perl scripts in directory specified at settings/scripts.\n"
					+ "		Run scripts: runs selected scripts on active ttcn file.\n"
					+ "		Note that all chosen files need to be in workspace.\n"
					+ "\nTemplates:\n"
					+ "		TCs List: path to file which comprises listed test cases. In file, every test case starts with new line."
					+ " In SD it' s acceptable to numerate lines with signs \"1.\", \"8)\", \"42\" or without any character, while in "
					+ " MD it' s crucial to make it by \"1.3\", \"3.4\".\n"
					+ "		Feature number: number, stored as int.\n"
					+ "		Feature slogan: short description of feauture.\n"
					+ "		TD's template: optional path to td's template's pattern file. If not specified, the default one will be used.\n"
					+ "		Create: creates template in src directory.\n"
					+ "		Create in: creates template in directory chosen by user.\n"
					+ "\nSettings:\n"
					+ "		Scripts: path to directory with pyhon&perl scripts.\n"
					+ "		Python: path to python.exe\n"
					+ "		Perl: path to perl.exe\n"
					+ "		Update: updates main tab with files in chosen directory\n"
					+ "		Make Default: saves and updates all 3 paths as default values.";
	
	
	
	public Composite getContent(TabFolder parent, Display display){
		mainComposite = new Composite(parent, SWT.WRAP);
		mainComposite.setLayout(new MigLayout("fill"));
		black = display.getSystemColor(SWT.COLOR_BLACK);
		white = display.getSystemColor(SWT.COLOR_WHITE);
		logoPath = CoreSupport.getPath("resources/80.png");
		ranges = new StyleRange[17];
		
		logo = new Image(display, logoPath);
		logoLabel = new Label(mainComposite, SWT.NONE);
		logoLabel.setImage(logo);
		
		content = new StyledText(mainComposite, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);

		scrollBarListener = new Listener (){
			public void handleEvent(Event event) {
		    	StyledText t = (StyledText)event.widget;
		        Rectangle r1 = t.getClientArea();
		        Rectangle r2 = t.computeTrim(r1.x, r1.y, r1.width, r1.height); 
		        Point p = t.computeSize(r1.x,  SWT.DEFAULT,  true); 
		        t.getVerticalBar().setVisible(r2.height <= p.y);
		        if (event.type == SWT.Modify){
		           t.getParent().layout(true);
		        t.showSelection();
	        }
		}};
		
		content.addListener(SWT.Resize, scrollBarListener);
		content.addListener(SWT.Modify, scrollBarListener);
		
		content.setText(text);
		
		ranges[0] = new StyleRange(text.indexOf("Main:"), "Main:".length(), black, white, SWT.BOLD);
		ranges[1] = new StyleRange(text.indexOf("Listed scripts:"), "Listed scripts:".length(), black, white, SWT.BOLD);
		ranges[2] = new StyleRange(text.indexOf("Run scripts:"), "Run Files:".length(), black, white, SWT.BOLD);
		ranges[3] = new StyleRange(text.indexOf("Note that all chosen files need to be in workspace."), "Note that all chosen files need to be in workspace.".length(), black, white, SWT.BOLD);
		ranges[4] = new StyleRange(text.indexOf("Templates:"), "Templates:".length(), black, white, SWT.BOLD);
		ranges[5] = new StyleRange(text.indexOf("TCs List:"), "TCs List:".length(), black, white, SWT.BOLD);
		ranges[6] = new StyleRange(text.indexOf("Feature number:"), "Feature Number:".length(), black, white, SWT.BOLD);
		ranges[7] = new StyleRange(text.indexOf("Feature slogan:"), "Feature slogan:".length(), black, white, SWT.BOLD);
		ranges[8] = new StyleRange(text.indexOf("TD's template:"), "Feature slogan:".length(), black, white, SWT.BOLD);
		ranges[9] = new StyleRange(text.indexOf("Create:"), "Create:".length(), black, white, SWT.BOLD);
		ranges[10] = new StyleRange(text.indexOf("Create in:"), "Create in:".length(), black, white, SWT.BOLD);
		ranges[11] = new StyleRange(text.indexOf("Settings:"), "Settings:".length(), black, white, SWT.BOLD);
		ranges[12] = new StyleRange(text.indexOf("Scripts:"), "Scripts:".length(), black, white, SWT.BOLD);
		ranges[13] = new StyleRange(text.indexOf("Python:"), "Python:".length(), black, white, SWT.BOLD);
		ranges[14] = new StyleRange(text.indexOf("Perl:"), "Perl:".length(), black, white, SWT.BOLD);
		ranges[15] = new StyleRange(text.indexOf("Update:"), "Update:".length(), black, white, SWT.BOLD);
		ranges[16] = new StyleRange(text.indexOf("Make Default:"), "Make Default:".length(), black, white, SWT.BOLD);
		
	    content.replaceStyleRanges(0, text.length()-5, ranges);
		logoLabel.setLayoutData("al 50% 0%, wrap");
		content.setLayoutData("pos 0% 25% 100% 100%, wmin 0");
		return mainComposite;
	}
}
