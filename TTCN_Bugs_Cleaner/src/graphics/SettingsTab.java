package graphics;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Text;

import net.miginfocom.swt.MigLayout;

public class SettingsTab {
	Composite mainComposite;
    Label scriptsDirLabel;
    Label pythonDirLabel;
    Label perlDirLabel;
    Text scriptsDir;
    Text pythonDir;
    Text perlDir;
    Button scriptsDirButton;
    Button pythonDirButton;
    Button perlDirButton;
    Button update;
    Button makeDefault;
    
	public Composite getContent( final Shell shell, TabFolder parent, final Model model ){
		mainComposite = new Composite(parent, SWT.WRAP);
		mainComposite.setLayout(new MigLayout("fillx", "[][grow][]"));
		
		scriptsDirLabel = new Label(mainComposite, SWT.NONE); //Scripts
		scriptsDir = new Text(mainComposite, SWT.BORDER);
		scriptsDirButton = new Button(mainComposite, SWT.PUSH);
		pythonDirLabel = new Label(mainComposite, SWT.NONE);
		pythonDir = new Text(mainComposite, SWT.BORDER);
		pythonDirButton = new Button(mainComposite, SWT.NONE);
		perlDirLabel = new Label(mainComposite, SWT.NONE);
		perlDir = new Text(mainComposite, SWT.BORDER);
		perlDirButton = new Button(mainComposite, SWT.NONE);
		update = new Button(mainComposite, SWT.PUSH);
		makeDefault = new Button(mainComposite, SWT.PUSH);
		
		scriptsDirLabel.setText("Scripts:");
		pythonDirLabel.setText("Python:");
		perlDirLabel.setText("Perl:");
		scriptsDirButton.setText("...");
		pythonDirButton.setText("...");
		perlDirButton.setText("...");
		update.setText("Update");
		makeDefault.setText("Make Default");
		
		scriptsDir.setLayoutData("growx, wmin 0");
		scriptsDirButton.setLayoutData("wrap");
		pythonDir.setLayoutData("growx, wmin 0");
		pythonDirButton.setLayoutData("wrap");
		perlDir.setLayoutData("growx, wmin 0");
		perlDirButton.setLayoutData("wrap");
		update.setLayoutData("cell 0 3 3 1, al 50%");
		makeDefault.setLayoutData("cell 0 3 3 1, al 50%");

		scriptsDirButton.addSelectionListener(new DirectoryBrowser(scriptsDir));
		pythonDirButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				//dialog.setFilterExtensions(new String [] {"*.exe"});
				String result = dialog.open();
				if ( result != null ) 
					pythonDir.setText(result);
			}
		});
		
		perlDirButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				//dialog.setFilterExtensions(new String [] {"*.exe"});
				String result = dialog.open();
				if ( result != null ) 
					perlDir.setText(result);
			}
		});
		update.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				updatePaths(model);
			}
		});

		makeDefault.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if ( updatePaths(model) )
					model.saveDefaults( getScriptsDirValue(), getPythonDirValue(), getPerlDirValue() );
			}
		});
		
		return mainComposite;
	}
	
	
	public boolean updatePaths(Model model){
		//Updates scripts source for scripts list in mainTab and paths to Python and Perl
		boolean correctPaths = true;
		try{
			if( (new File( getScriptsDirValue() )).exists() ){
				model.scriptsPathSetter( getScriptsDirValue() );
				model.updateScripts( getScriptsDirValue() );
			}
			
			else {
				MessageDialog.showMessage("Scripts path invalid.");
				correctPaths = false;
			}
			
			if( (new File( getPythonDirValue() )).exists() && getPythonDirValue().contains("python") )
				model.pythonPathSetter( getPythonDirValue() );
			
			else{
				MessageDialog.showMessage("Python path invalid.");
				correctPaths = false;
			}
			
			if( (new File( getPerlDirValue() )).exists()  && getPerlDirValue().contains("perl") )
				model.perlPathSetter( getPerlDirValue() );
			
			else{
				MessageDialog.showMessage("Perl path invalid.");
				correctPaths = false;
			}
				
		} catch(ClassCastException e){
			System.out.println(e);
		}
		return correctPaths;
	}
	
	/**
	 * Reads Defaults.txt file and sets paths to Texts in settingsTab
	 */
	public void updatePathsInSettings( String ScriptsDirValue, String PythonDirValue, String PerlDirValue){
		setScriptsDirValue(ScriptsDirValue);
		setPythonDirValue(PythonDirValue);
		setPerlDirValue(PerlDirValue);
	}
	
	public String getScriptsDirValue(){
		return ((Text) scriptsDir).getText();
	}
	
	public String getPythonDirValue(){
		return ((Text) pythonDir).getText(); 
	}
	
	public String getPerlDirValue(){
		return ((Text) perlDir).getText();
	}
	
	public void setScriptsDirValue(String value){
		scriptsDir.setText(value);
	}
	
	public void setPythonDirValue(String value){
		pythonDir.setText(value);
	}
	
	public void setPerlDirValue(String value){
		perlDir.setText(value);
	}
}
