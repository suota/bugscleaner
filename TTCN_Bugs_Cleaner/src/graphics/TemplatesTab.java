package graphics;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Text;

import handlers.CoreSupport;
import handlers.TemplateSupport;
import net.miginfocom.swt.MigLayout;

public class TemplatesTab {
	Composite mainComposite;

    //Standard Development components
    Group groupSD;
    Label listOfTcsSDLabel;
    Label featureNumberSDLabel;
    Label featureSloganSDLabel;
    Label tdPatternSDLabel;
    Text listOfTcsSD;
    Text featureNumberSD;
    Text featureSloganSD;
    Text tdPatternSD;
    Button browseListOfTcsSD;
    Button createTDSD;
    Button createTDSDIn;
    Button browseTdPatternSD;
    
    //Market Development components
    Group groupMD;
    Label listOfTcsMDLabel;
    Label featureNumberMDLabel;
    Label featureSloganMDLabel;
    Label tdPatternMDLabel;
    Text listOfTcsMD;
    Text featureNumberMD;
    Text featureSloganMD;
    Text tdPatternMD;
    Button browseListOfTcsMD;
    Button createTDMD;
    Button createTDMDIn;
    Button browseTdPatternMD;
    
    
	public Composite getContent(TabFolder parent, final Shell shell){
		mainComposite = new Composite(parent, SWT.NONE);
		mainComposite.setLayout(new MigLayout("fillx, filly"));

		/*Standard Development*/
		groupSD = new Group(mainComposite, SWT.SHADOW_ETCHED_IN);
		groupSD.setText("Standard Development");
		groupSD.setLayoutData("pos 0% 0% 100% 50%");
		groupSD.setLayout(new MigLayout("fillx", "[][grow][]"));
		
		listOfTcsSDLabel = new Label(groupSD, SWT.NONE);
		listOfTcsSD = new Text(groupSD, SWT.BORDER);
		browseListOfTcsSD = new Button(groupSD, SWT.PUSH);
		featureNumberSDLabel = new Label(groupSD, SWT.NONE);
		featureNumberSD = new Text(groupSD, SWT.BORDER);
		featureSloganSDLabel = new Label(groupSD, SWT.NONE);
		featureSloganSD = new Text(groupSD, SWT.BORDER);
		tdPatternSDLabel = new Label(groupSD, SWT.NONE);
		tdPatternSD = new Text(groupSD, SWT.BORDER);
		browseTdPatternSD = new Button(groupSD, SWT.PUSH);
		createTDSD = new Button(groupSD, SWT.PUSH);
		createTDSDIn = new Button(groupSD, SWT.PUSH);
		
		listOfTcsSDLabel.setText("TCs list:");
		featureNumberSDLabel.setText("Feature number:");
		featureSloganSDLabel.setText("Feature slogan:");
		browseListOfTcsSD.setText("...");
		tdPatternSDLabel.setText("TD's template:");
		tdPatternSD.setText("Optional");
		browseTdPatternSD.setText("...");
		createTDSD.setText("Create");
		createTDSDIn.setText("Create in");
		
		listOfTcsSDLabel.setLayoutData("");
		listOfTcsSD.setLayoutData("growx");
		browseListOfTcsSD.setLayoutData("wrap");
		featureNumberSDLabel.setLayoutData("");
		featureNumberSD.setLayoutData("span, growx, wrap");
		featureSloganSDLabel.setLayoutData("");
		featureSloganSD.setLayoutData("span, growx, wrap");
		tdPatternSDLabel.setLayoutData("");
		tdPatternSD.setLayoutData("growx");
		browseTdPatternSD.setLayoutData("wrap");
		createTDSD.setLayoutData("cell 0 4 3 1, al 50%");
		createTDSDIn.setLayoutData("cell 0 4 3 1, al 50%");
		
		browseListOfTcsSD.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.setFilterExtensions(new String [] {"*.ttcn; *.txt", "*.*"});
				String result = dialog.open();
				if ( result != null ) 
					listOfTcsSD.setText(result);
			}
		});
		
		createTDSD.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				try{
					String pathToTemplate=CoreSupport.getFolder("GeneratedTemplates",CoreSupport.getFirstProjectPath());
					TemplateSupport MyTemplateSupport=new TemplateSupport();
					MyTemplateSupport.CreateSDTemplate(featureSloganSD.getText(), featureNumberSD.getText(),listOfTcsSD.getText(),pathToTemplate,tdPatternSD.getText());
				}catch(NullPointerException e){
					MessageDialog.showMessage("No open project in the workspace");
				}
			}
		});
		
		browseTdPatternSD.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.setFilterExtensions(new String [] {"*.ttcn; *.txt", "*.*"});
				String result = dialog.open();
				if ( result != null ) 
					tdPatternSD.setText(result);
			}
		});
		
		createTDSDIn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				DirectoryDialog dlg = new DirectoryDialog(shell);
				dlg.setMessage("Where should generated template be saved?");
				try{
					dlg.setFilterPath(CoreSupport.getFirstProjectPath());
					String pathToDir = dlg.open();
					if(pathToDir != null){
						TemplateSupport MyTemplateSupport = new TemplateSupport();
						MyTemplateSupport.CreateSDTemplate(featureSloganSD.getText(), featureNumberSD.getText(),
															listOfTcsSD.getText(), pathToDir, tdPatternSD.getText() );
					}
				} catch(NullPointerException e){
					MessageDialog.showMessage("No open project in the workspace");
				}
				
		}
	});
		
		
		
		/*Market Development*/
		
		groupMD = new Group(mainComposite, SWT.SHADOW_ETCHED_IN);
		groupMD.setText("Market Development");
		groupMD.setLayoutData("pos 0% 50% 100% 100%");
		groupMD.setLayout(new MigLayout("fillx", "[][grow][]"));
		
		listOfTcsMDLabel = new Label(groupMD, SWT.NONE);
		listOfTcsMD = new Text(groupMD, SWT.BORDER);
		browseListOfTcsMD = new Button(groupMD, SWT.PUSH);
		featureNumberMDLabel = new Label(groupMD, SWT.NONE);
		featureNumberMD = new Text(groupMD, SWT.BORDER);
		featureSloganMDLabel = new Label(groupMD, SWT.NONE);
		featureSloganMD = new Text(groupMD, SWT.BORDER);
		tdPatternMDLabel = new Label(groupMD, SWT.NONE);
		tdPatternMD = new Text(groupMD, SWT.BORDER);
		browseTdPatternMD = new Button(groupMD, SWT.PUSH);
		createTDMD = new Button(groupMD, SWT.PUSH);
		createTDMDIn = new Button(groupMD, SWT.PUSH);
		
		listOfTcsMDLabel.setText("TCs list:");
		featureNumberMDLabel.setText("Feature number:");
		featureSloganMDLabel.setText("Feature slogan:");
		browseListOfTcsMD.setText("...");
		tdPatternMDLabel.setText("TD's template:");
		tdPatternMD.setText("Optional");
		browseTdPatternMD.setText("...");
		createTDMD.setText("Create");
		createTDMDIn.setText("Create in");
		
		listOfTcsMD.setLayoutData("growx");
		browseListOfTcsMD.setLayoutData("wrap");
		featureNumberMD.setLayoutData("span, growx, wrap");
		featureSloganMD.setLayoutData("span, growx, wrap");
		tdPatternMDLabel.setLayoutData("");
		tdPatternMD.setLayoutData("growx");
		browseTdPatternMD.setLayoutData("wrap");
		createTDMD.setLayoutData("cell 0 4 3 1, al 50%");
		createTDMDIn.setLayoutData("cell 0 4 3 1, al 50%");
		
		browseListOfTcsMD.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.setFilterExtensions(new String [] {"*.ttcn; *.txt", "*.*"});
				String result = dialog.open();
				if ( result != null ) 
					listOfTcsMD.setText(result);
			}
		});
		
		browseTdPatternMD.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.setFilterExtensions(new String [] {"*.ttcn; *.txt", "*.*"});
				String result = dialog.open();
				if ( result != null ) 
					tdPatternMD.setText(result);
			}
		});
		
		createTDMD.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				TemplateSupport myTemplateSupport = new TemplateSupport();
				try{
					String pathToTemplate=CoreSupport.getFolder("GeneratedTemplates",CoreSupport.getFirstProjectPath());
					myTemplateSupport.CreateMDTemplate(featureSloganMD.getText(), featureNumberMD.getText(),listOfTcsMD.getText(),pathToTemplate,tdPatternMD.getText());
				}catch(NullPointerException e){
					MessageDialog.showMessage("No open project in the workspace");
				}
			}
		});
		
		createTDMDIn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				DirectoryDialog dlg = new DirectoryDialog(shell);
				
				dlg.setMessage("Where should generated template be saved?");
				try{
					dlg.setFilterPath(CoreSupport.getFirstProjectPath());
					String pathToDir = dlg.open();
					TemplateSupport myTemplateSupport=new TemplateSupport();
					myTemplateSupport.CreateMDTemplate(featureSloganMD.getText(), featureNumberMD.getText(),listOfTcsMD.getText(),pathToDir,tdPatternMD.getText());}catch(NullPointerException e){
						MessageDialog.showMessage("No open project in the workspace");
				}
			}
		});

		return mainComposite;
	}
}
