package graphics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import handlers.CoreSupport;
import handlers.MarkerSupport;
import launchers.DistributorLauncher;

/**
 * Model class with most important global variables - paths with setters and getters.
 */
public class Model {
	View view;
	private String scriptsPath;
	private String pythonPath;
	private String perlPath;
	private String [] lastScripts;
	
	public void setView(View view){
		this.view = view;
	}
	
	public void runScripts( Object[] listedScripts){
		try{
			MarkerSupport mySupport = new MarkerSupport();
			String [] scriptsPaths = new String[listedScripts.length];
			
			for (int i=0; i < listedScripts.length; i++) 
				scriptsPaths[i] = listedScripts[i].toString();
			
			DistributorLauncher myDis = new DistributorLauncher(
							pythonPathGetter(),
							perlPathGetter(), 
							mySupport.getPathToActiveIFile(), scriptsPaths);
			
			ArrayList<String[]> warnings = myDis.distribute();
			mySupport.markAllWarnings( warnings );
			
			view.startEndingView( myDis.getErrors() , warnings.size() );
			
		}catch(NullPointerException e){
			MessageDialog.showMessage("No active file in the editor");
		}
		
	}	
	
	public void loadDefaults(){
		File f = CoreSupport.getFile("data/Defaults.txt");
		Scanner in;
		ArrayList <String> tmpLastScripts = new ArrayList<String>();
		try {
			in = new Scanner(f);
			String s = in.nextLine();
			
			while(!s.contains("DefaultPaths")){	//search for first DefaultPaths label
				s = in.nextLine();
			}
			
			scriptsPathSetter( in.nextLine() );	//due to our convention
			pythonPathSetter( in.nextLine() );	//we are sure
			perlPathSetter( in.nextLine() );	//they are here
			
			while(!s.contains("LastScripts")){
				s = in.nextLine();
			}
			s = in.nextLine();	//jump to line after first LastScripts
			while(! s.contains("LastScripts")){
				tmpLastScripts.add( s );
				s = in.nextLine();
			}
			in.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		lastScripts = tmpLastScripts.toArray(new String[0]);
		
		if( new File( scriptsPathGetter() ).exists())	//sets input for main tab list of scripts
			view.updateScripts( scriptsPathGetter() );
		
		view.settingsTab.updatePathsInSettings( scriptsPathGetter(), pythonPathGetter(), perlPathGetter());
	}
	
	public void saveDefaults(String scriptsPath, String pythonPath, String perlPath){
		
		PrintWriter pwr;
		Scanner scan;
		File f = CoreSupport.getFile("data/Defaults.txt");
		String[] paths = {scriptsPath, pythonPath, perlPath}; 
		ArrayList <String> tmpLastScripts = new ArrayList<String>();
		try {
			scan = new Scanner(f);
			String s = scan.nextLine();
			while(!s.contains("LastScripts")){
				s = scan.nextLine();
			}
			s = scan.nextLine();	//jump to line after first LastScripts
			while(!s.contains("LastScripts")){
				tmpLastScripts.add( s );
				s = scan.nextLine();
			}
			
			scan.close();
		} catch(FileNotFoundException e ){
			e.printStackTrace();
		}
		
		/*Saves new paths to file, but firstly checks if they exist */
		try{	
			pwr = new PrintWriter(f);
			pwr.println("DefaultPaths");
			for(String line : paths) {
				if(new File(line).exists() && (!line.equals(scriptsPath)&&(line.contains("python")||line.contains("perl")))|| line.equals(scriptsPath)){
					pwr.println(line);
				}
				else {
					pwr.println();
				}
			}
			pwr.println("DefaultPaths");
			pwr.println();
			pwr.println("LastScripts");
			for( int i = 0 ; i < tmpLastScripts.size() ; i++)
				pwr.println(tmpLastScripts.get(i));
			pwr.print("LastScripts");
			pwr.close();
			
		} catch(FileNotFoundException e ){
			e.printStackTrace();
		}
		
		scriptsPathSetter(scriptsPath);
		pythonPathSetter(pythonPath);
		perlPathSetter(perlPath);
	}
	

	public void saveDefaults(Object[] checkedScripts){
		
		String [] runnedScripts = new String[ checkedScripts.length ];
		String [] defaultPath = new String[5];
		File f = CoreSupport.getFile("data/Defaults.txt");
		PrintWriter pwr;
		Scanner scan;
		
		
		for (int i = 0; i < checkedScripts.length; i++) 
			runnedScripts[i] = checkedScripts[i].toString();
		
		try {
			scan = new Scanner(f);
			for ( int i = 0 ; i < 5 ; i++)
				defaultPath[i] = scan.nextLine();
			
			scan.close();
			
			pwr = new PrintWriter(f);
			for(String line : defaultPath) {
				pwr.println(line);
			}
			pwr.println();
			pwr.println("LastScripts");
			for(String script : runnedScripts)
				pwr.println(script);
			
			pwr.println("LastScripts");
			pwr.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public void updateScripts (String scriptsPath){
		view.updateScripts(scriptsPath);
	}
	
	/**
	 * Getter for LastScripts
	 */
	public String[] LastScriptsGetter(){
		return this.lastScripts;
	}
	
	/**
	 * Getter for scriptsPath
	 */
	public String scriptsPathGetter(){
		return this.scriptsPath;
	}

	/**
	 * Getter for pythonPath
	 */
	public String pythonPathGetter(){
		return this.pythonPath;
	}

	/**
	 * Getter for perlPath
	 */
	public String perlPathGetter(){
		return this.perlPath;
	}
	
	/**
	 * Setter for scriptsPath
	 * @param scriptsPath - this value would be set for scriptsPath variable.
	 */
	public void scriptsPathSetter(String scriptsPath){
		this.scriptsPath = scriptsPath;
	}
	
	/**
	 * Setter for pythonPath
	 * @param pythonPath - this value would be set for pythonPath variable.
	 */
	public void pythonPathSetter(String pythonPath){
		this.pythonPath = pythonPath;
	}
	
	/**
	 * Setter for perlPath
	 * @param perlPath - this value would be set for perlPath variable.
	 */
	public void perlPathSetter(String perlPath){
		this.perlPath = perlPath;
	}
}