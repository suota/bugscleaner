package graphics;

import java.io.File;
import java.io.FileFilter;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * This class returns the files in the specified directory. If the specified
 * directory doesn't exist, it returns an empty array.
 */

public class ScriptsContentProvider implements IStructuredContentProvider {
	  private static final Object[] EMPTY = new Object[] {"a", "b", "c"};

	  /**
	   * Gets the files in the specified directory
	   * 
	   * @param arg0
	   *            a String containing the directory
	   */
	  public Object[] getElements(Object arg0) {
	    File file = new File((String) arg0);
	    if (file.isDirectory()) {
	      return file.listFiles(new FileFilter() {
	        public boolean accept(File pathName)  {
	        	if(pathName.isFile() == false) { // Ignore directories; return only files
	        		return false;
	        	}
	        	boolean result = true;  
	        	String fileName = pathName.getName(); //Getting the name of the file 
	        	result = (fileName.endsWith(".py") || fileName.endsWith(".pl")); //Checking files extension
	          return result;
	        }
	      });
	    }
	    return EMPTY;
	  }

	  /**
	   * Disposes any created resources
	   */
	  public void dispose() {
	    // Nothing to dispose
	  }

	  /**
	   * Called when the input changes
	   */
	  public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
	    // Nothing to do
	  }
	}