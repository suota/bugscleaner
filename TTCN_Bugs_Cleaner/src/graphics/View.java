package graphics;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.TableItem;

import net.miginfocom.swt.MigLayout;


/**
 * View class as part of MVC. 
 * It initiates directly MainTab and also receives rest of the tabs from: TamplestesTab, SettingsTab and AboutTab.
 * It also contains methods for creating ErrorsView (which shows after running scripts if needed) and MessageDialog for various exceptions.
 * The whole GUI is created using SWT technology.
 */
public class View {
	Shell shell;
	Model model;
	Dialog mainDialog;
	SettingsTab settingsTab;
	CheckboxTableViewer scripts;
	int dialogWidth = 400;
    int dialogHeight = 500;
    
    
	public View( final Shell shell, Model model ){
		this.shell = shell;
		this.model = model;
	}
	
	
	public void startView(){
		mainDialog = new Dialog(shell){

			@Override
			protected void setShellStyle(int newShellStyle) {           
			    super.setShellStyle(SWT.CLOSE | SWT.MODELESS| SWT.BORDER | SWT.TITLE);
			    setBlockOnOpen(false);
			}
			
			@Override
			protected void handleShellCloseEvent(){
				setReturnCode(CANCEL);
				close();
				app.Activator.Running = true;
			}
			
			@Override
			protected Control createDialogArea(Composite parent) {
				
				Composite mainContainer = (Composite) super.createDialogArea(parent);	//like contentPane in JFrame
				mainContainer.setLayout(new FillLayout());
				TabFolder tabFolder = new TabFolder (mainContainer, SWT.BORDER);					//Now add cards (StackLayout) to mainCointainer
				
				//Runs methods that sets up particular tabs.
			    mainInit(tabFolder);
			    temaplatesInit(tabFolder);
			    settingsInit(tabFolder);
			    aboutInit(tabFolder);
			    model.loadDefaults();
			    return mainContainer;
			}
			
			/**
			* Sets up mainTab with all proper widgets and their functions.
			*/
			public void mainInit(TabFolder tabFolder){
				Composite mainComposite;
				Composite mainButtonsComposite;
			    final Button selectAll;
			    Button selectPrevious;
			    Button run;
			    
				TabItem mainTabItem = new TabItem (tabFolder, SWT.NONE);
				mainTabItem.setText("Main");
				mainComposite = new Composite(tabFolder, SWT.NONE);
				mainComposite.setLayout(new MigLayout("fill"));

				scripts = CheckboxTableViewer.newCheckList(mainComposite, SWT.FULL_SELECTION);
				scripts.setContentProvider(new ScriptsContentProvider());
				scripts.setLabelProvider(new ScriptsLabelProvider());
				
				mainButtonsComposite = new Composite(mainComposite, SWT.NONE);
				mainButtonsComposite.setLayout(new MigLayout("fill"));
				
				selectAll = new Button(mainButtonsComposite, SWT.CHECK);
				selectPrevious = new Button(mainButtonsComposite, SWT.CHECK);
				run = new Button(mainButtonsComposite, SWT.PUSH);
				selectAll.setText("Select all");
				selectPrevious.setText("Select previously used");
				run.setText("Run scripts");
				
				scripts.getTable().setLayoutData("pos 0% 0% 100% 85%, hmin 0");	//thanks to hmin 0 the scrollbar will appear and component won't stretch out of gui
				mainButtonsComposite.setLayoutData("pos 0% 85% 100% 100%");
				selectAll.setLayoutData("cell 0 1 1 1");
				selectPrevious.setLayoutData("cell 1 1 2 1");
				run.setLayoutData("cell 0 2 2 1");
				
				scripts.getTable().addListener(SWT.Selection, new Listener() {
					public void handleEvent(Event event) {
						if(event.detail != SWT.CHECK){
							TableItem ti  = (TableItem)event.item;
							ti.setChecked(!ti.getChecked());
						}
					}
				});
				
				selectAll.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent event) {
						if(selectAll.getSelection())
							scripts.setAllChecked(true);
						else
							scripts.setAllChecked(false);
					}
				});
				
				selectPrevious.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent event) {
						setLastUsedScripts( model.LastScriptsGetter() );
					}
				});
				
				run.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent event) {
						Object[] checkedScripts = scripts.getCheckedElements();
						
						model.runScripts( checkedScripts );
						model.saveDefaults( checkedScripts );
					}
				});
				
				mainTabItem.setControl(mainComposite);
			}

			
			/**
			* Sets up templatesTab.
			*/
			public void temaplatesInit(TabFolder tabFolder){
				TabItem templatesTabItem = new TabItem (tabFolder, SWT.NONE);
				templatesTabItem.setText("Templates");
				TemplatesTab templatesTab = new TemplatesTab();
				templatesTabItem.setControl(templatesTab.getContent(tabFolder, shell));
			}
			
			/**
			* Sets up settingsTab.
			*/
			public void settingsInit(TabFolder tabFolder){
				TabItem settingsTabItem = new TabItem (tabFolder, SWT.NONE);
				settingsTabItem.setText("Settings");
				settingsTab = new SettingsTab();
				
				settingsTabItem.setControl(settingsTab.getContent( shell, tabFolder, model ));
			}
			
			/**
			* Sets up aboutTab.
			*/
			public void aboutInit(TabFolder tabFolder){
				TabItem aboutTabItem = new TabItem (tabFolder, SWT.NONE);
				aboutTabItem.setText("About");
				AboutTab aboutTab = new AboutTab();
				aboutTabItem.setControl( aboutTab.getContent( tabFolder, shell.getDisplay() ) );
			}
			
			
			/**
			* Sets up last used scripts as selected
			*/
			public void setLastUsedScripts( String [] lastScripts ){
				TableItem[] children = scripts.getTable().getItems();
				for( int i = 0; i < lastScripts.length ; i++)
					for(int j = i ; j < children.length; j++)
						if( children[j].getData().equals( new File(lastScripts[i]) ) )
							children[j].setChecked(true);
			}
			
			@Override
			protected void configureShell(Shell newShell) {
				super.configureShell(newShell);
				newShell.setText("TTCN Bugs Cleaner");
			}
			 
			@Override
			protected boolean isResizable() {
				return true;
			}
			
			@Override
			protected void createButtonsForButtonBar(final Composite parent){ 
				GridLayout layout = (GridLayout)parent.getLayout();
				layout.marginHeight = 0;
			}
			
			@Override
			protected Point getInitialSize() {
				return new Point(dialogWidth, dialogHeight);
			}
			
		};
		
		mainDialog.open();
	}
	
	
	public void updateScripts (String scriptsPath){
		scripts.setInput(scriptsPath);
	}
	
	
	
	/**
	 * Creates and opens up dialog with information about problems with running scripts on files.
	 * It's invoked only if there were any.
	 * @param errors ArrayList with errors which scripts had with analysing files. Every array provides 3 informations: What file, what script and short description 
	 */
	
	public void startEndingView(final ArrayList <String[]> errors, final int numberOfWarnings){
		

		Dialog errorDialog = new Dialog(shell){
			
			int dialogWidth;
			int dialogHeight;
			
			@Override
			protected Control createDialogArea(Composite parent) {
				
				Composite mainContainer = (Composite) super.createDialogArea(parent);	//like contentPane in JFrame
				StyledText content;
				String text = "";
				Listener scrollBarListener;
				Color black;
				Color white;
				
				
				mainContainer.setLayout( new FillLayout() );
				content = new StyledText(mainContainer, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
				
				scrollBarListener = new Listener (){
					public void handleEvent(Event event) {
				    	StyledText t = (StyledText)event.widget;
				        Rectangle r1 = t.getClientArea();
				        Rectangle r2 = t.computeTrim(r1.x, r1.y, r1.width, r1.height); 
				        Point p = t.computeSize(r1.x,  SWT.DEFAULT,  true); 
				        t.getVerticalBar().setVisible(r2.height <= p.y);
				        if (event.type == SWT.Modify){
				           t.getParent().layout(true);
				        t.showSelection();
			        }
				}};
				content.addListener(SWT.Resize, scrollBarListener);
				content.addListener(SWT.Modify, scrollBarListener);
				
				if( !errors.isEmpty() ){
					black = shell.getDisplay().getSystemColor(SWT.COLOR_BLACK);
					white = shell.getDisplay().getSystemColor(SWT.COLOR_WHITE);
					String [] signs = new String[]{ "File:", "Script:", "Information:" };
					for(int i = 0; i < errors.size(); i++){
						for(int j = 0; j < signs.length; j++){
					    	text += signs[j] + "\n";
						   	text += errors.get(i)[j] + "\n";
					    }
					    text += "\n";
					}
					
					content.setText(text);
					    
				    for(int i = 0 ; i< signs.length; i++){
				    	int index = text.indexOf(signs[i]);
				    	while (index >= 0) {
				    		content.setStyleRange(new StyleRange( index, signs[i].length(), black, white, SWT.BOLD));
				    	    index = text.indexOf(signs[i], index + 1);
				    	}
				    }
				    dialogWidth = 600;
				    dialogHeight = 300;
				}
				
				else{
					text = "The plugin finished job.\nThere were: " + numberOfWarnings + " warnings";
					content.setText(text);
					dialogWidth = 300;
				    dialogHeight = 200;
				}
				return mainContainer;
			}
			
			@Override
			protected void configureShell(Shell newShell) {
				super.configureShell(newShell);
				newShell.setText("TTCN Bugs Cleaner");
			}
			
			@Override
			  protected void createButtonsForButtonBar(Composite parent) {
			    createButton(parent, IDialogConstants.OK_ID, "I'm feeling lucky", true);
			  }
			
			@Override
			protected void okPressed(){
				this.close();
				mainDialog.close();
				app.Activator.Running = true;
			}
			
			@Override
			protected boolean isResizable() {
				return true;
			}
			
			@Override
			protected Point getInitialSize() {
				return new Point(dialogWidth, dialogHeight);
			}
		};
		errorDialog.open();
	}
}