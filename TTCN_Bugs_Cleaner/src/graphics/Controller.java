package graphics;

/**
 * Controller class with basic functionality. 
 */
public class Controller {
	Model model;
	View view;
	
	/**
	 * Class constructor with main Model and View.
	 */
	public Controller(Model model, View view){
		this.model = model;
		this.view = view;
	}
	
	/**
	 * Opens up plugin dialog.
	 */
	public void startView(){
		model.setView(view);
		view.startView();
	}	
}
