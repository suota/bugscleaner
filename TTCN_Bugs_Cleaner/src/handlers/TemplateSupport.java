package handlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import graphics.MessageDialog;

public class TemplateSupport{
	

	private String addZeros(int n,int numLines){ //helper method
		if(numLines<1000){
			if(n<10)  return "00" + n;
		    if(n<100) return "0" + n;
		    return "" + n;
		}
		else{
			if(n<10)  return "000" + n;
		    if(n<100) return "00" + n;
		    if(n<1000) return "0" + n;
		    return "" + n;
		}
	}
	
	private boolean isNumeric(String s) {  //helper method, which checks if Strings contain only digits
	    return s.matches("[-+]?\\d*\\.?\\d+");  
	}

	public ArrayList<String> SDSeperateTestCases (File file, String featureNumber) throws FileNotFoundException{

		Scanner in=null;
		Scanner numOfLines = null;
		ArrayList<String> result =new ArrayList<String>();
		int num = 0;
		
		try {
			numOfLines = new Scanner(file);
			while (numOfLines.hasNextLine())    
		    {
				if( !numOfLines.nextLine().isEmpty())
					num ++;		   
		    }
			
			in = new Scanner(file);
			String line ; 
			int lineNumber = 0;
			String splitted[];
			
			while(in.hasNextLine()){
				if(!(line = in.nextLine()).isEmpty()){	
				lineNumber++;
				splitted = line.split(" ");
				String joined = "";
				String first = splitted[0];
				
				int index = 0;
				
				while(index < first.length()&& !Character.isLetter(first.charAt(index)))
						index++;
			
				String sub = first.substring(index);
													
				if (!sub.isEmpty())
					joined+= "_"+ sub;
				
				for(int i = 1 ; i < splitted.length ; i++ ){
					if(!splitted[i].equals(" ") && !splitted[i].isEmpty())
					joined+= "_" +splitted[i];
				}
					
				result.add( "tc_TD" + featureNumber + "_" + addZeros(lineNumber,num) + joined);
			}
			}			
		} finally{
			if(in!=null)
				in.close();
			if(numOfLines!=null)
				numOfLines.close();
		}

		return result;
	}
	
	
	public ArrayList<String[]> MDSeperateTestCases (File file)throws FileNotFoundException{

		ArrayList<String[]> result =new ArrayList<String[]>();
		Scanner in=null;
		String line;
		
		try {
			in = new Scanner(file);
			while(in.hasNextLine()) {
				line = in.nextLine();
				boolean goodConvention = MDCheckConvention(line);
				
				if(goodConvention == true) {
					String[] splitted = line.split(" ");
					StringBuilder concat = new StringBuilder("");
					
					String toCheck = splitted[0];
					StringBuilder opening = new StringBuilder("");
					boolean Add = false;
					
					for(int i = 0; i < toCheck.length(); i++) {
						char a = toCheck.charAt(i);
						if(Add == true) {
							opening.append(a);
						}
						if (Add == false && (a < '0' || a > '9')&& a != '.' ) {
							opening.append(a); 
							Add = true;
						}
					}
					
					if(Add == true) {
						concat.append(opening);
						concat.append(" ");
					}
					
					for ( int i=1 ; i<splitted.length; i++){
						concat.append(splitted[i]);
						concat.append(" ");
					}
				
				String [] elem = {MDConvert(line), concat.toString()};
				result.add(elem);
				}
			
				else { //when we find line which not satisfy convention we print an error or do nothing if it contains only spaces
					if (line.matches(" +") || line.equals("")){}
					
					else {
					System.out.println("Zla konwercja zapisu w MD");
					return new ArrayList<String[]>();
					}
				}
			}
			}finally{
				if(in!=null)
					in.close();
			}
		return result;
	}
	
	public boolean MDCheckConvention (String s) {
		boolean isGood = true;
		if(s.matches(" +") || s.equals("")) {
			return false;
		}
		String[] spaces = s.split(" ");
		String[] dots = spaces[0].split(Pattern.quote("."));
		
		for(int i = 0; i < (dots.length - 1); i++) {
			if(isNumeric(dots[i]) == false) {
				isGood = false;
				break;
			}
		}
		
		if (isNumeric(dots[0]) == false) {
			isGood = false;
		}
		
		return isGood;	
		//isGood means that line satisfy convention - digits separated with dots, eventually with String on the last position,
		//but this String have some digits at the beggining
	}
	
	
	public String MDConvert(String s) {
		StringBuilder result = new StringBuilder("tc_B"); //converted line
		String[] spaces = s.split(" ");
		String[] dots = spaces[0].split(Pattern.quote("."));
				
		for(int i = 0; i < (dots.length - 1);i++) { //we know that line satisfy convention so all (exclusive of the last one) have to be digit
			result.append("_");
			result.append(dots[i]);
		}
		
		
		if (isNumeric(dots[dots.length -1])){ // if the last one is numeric
			result.append("_");
			result.append(dots[dots.length - 1]);
		}
		
		else { //if not, there still might be string with digit on the beginning
			String toCheck = dots[dots.length -1]; //the last element of array dots might have some digits at the beginning
			StringBuilder ending = new StringBuilder(); // potential ending of the String result
			boolean add = false; //Add will be true if loop will find digit at the beginning of string toCheck
			
			for(int i = 0; i < toCheck.length(); i++) {
				char a = toCheck.charAt(i);
				if (a >= '0' && a <= '9') {
					ending.append(a); 
					add = true;
				}
				
				else { //if we find non digit char we have to stop searching
					break;
				}
				
			}
			
			if(add == true) {
				result.append("_");
				result.append(ending);
			}	
		}
		
		return result.toString(); //converting StringBuilder to String	
	}
	
	
	/**Method that creates files and call writeMDTemplate to fill they.
	 * @param tdslogan Slogan of TD
	 * @param tdNumber Number of TD
	 * @param tcListPath Path to the list of tc provided by user
	 * @param templatePath path to the template program is creating
	 * @param patternPath path to the pattern (default or provided by user)
	 */
	public void CreateSDTemplate(String tdslogan,String tdNumber,String tcListPath,String templatePath,String patternPath){
		File tcFile = new File(tcListPath);
		ArrayList<String> tcNames;
		try {
			tcNames = this.SDSeperateTestCases(tcFile, tdNumber);
		} catch (FileNotFoundException e1) {
			MessageDialog.showMessage("File containing TC's list not found");
			return;
		}
		PrintWriter writer = null;
		Scanner in = null;
		String filename="";		
		try{
			filename = "TD" + tdNumber + "_" + tdslogan + ".ttcn"; //creating the filename
			writer = new PrintWriter(templatePath + "/" + filename, "UTF-8");
			if(patternPath.equals("Optional")) //Checking if we use default or custom path to pattern
				in = new Scanner(new FileReader(CoreSupport.getPath("materials/TEMPLATES/SD_TEMPLATE.ttcn"))); //defaultpath
			else
				in = new Scanner(new FileReader(patternPath)); //custompath
			writeSDTemplate(writer,in,tcNames,tdNumber,tdslogan); //filling the template
			CoreSupport.getFirstIProject().refreshLocal(IResource.DEPTH_INFINITE, null); // refreshing workspace
			
		} catch (FileNotFoundException e) {
			MessageDialog.showMessage("File with Template or TC's list not found");
		} catch (UnsupportedEncodingException e) {
			MessageDialog.showMessage("File with Template has wrong encoding");
		} catch (CoreException e){
			e.printStackTrace();
		} catch (NullPointerException e){
			MessageDialog.showMessage("File with Template or TC's list not found");
		}
		
		finally{ //close the resources
			if(in!=null)
				in.close();
			if(writer!=null){
				writer.close();
			}
		}
			openInEditor(templatePath+"/"+filename,CoreSupport.getFirstIProject()); //opening file in default editor
	}
	/**Method that fills the template with text
	 * @param writer PrintWriter of the desired to write fileto write on the desired file
	 * @param in Scanner to read text from pattern file
	 * @param tcNames ArrayList of Names of TestCases
	 * @param tdNumber Number of td
	 * @param tdSlogan slogan of td
	 */
	
	private void writeSDTemplate(PrintWriter writer,Scanner in,ArrayList<String>tcNames,String tdNumber,String tdSlogan){
		while(in.hasNextLine())	{
			String currentLine=in.nextLine();
			
			if(currentLine.equals("#TC_START")){ //Start of custom for eatch tc block
				ArrayList<String>customPart=new ArrayList<String>(); //create the new ArrayList to store custom part
					currentLine=in.nextLine();
				while(!currentLine.equals("#TC_END")){ //while not end
						customPart.add(currentLine+"\n"); //copy the lines to the custom part ArrayList
						currentLine=in.nextLine();
						}
				int number=0;
				for(String tcName: tcNames){
					writeCustomTCPartforSDTemplate(writer,tcName,customPart,tdNumber,tdSlogan,number++); //write part custom to each tc
					
				}
			}
			else
				writer.write(currentLine.replaceAll("AAAA", tdNumber).replaceAll("slogan", tdSlogan));
				writer.write("\n");
		}	
	}
	
	/**method that generates the custom to each tc's part and writes it in the file
	 * @param writer PrintWriter ,which writes on the desired file
	 * @param tcName Number of the tc separated by '_'
	 * @param CustomPart ArrayList of strings that create custom to all tc part
	 * @param tdNumber Number of td
	 * @param tdSlogan  Slogan of the td
	 * @param caseNumber Number needed to replace $caseNumber in switch
	 */
	private void writeCustomTCPartforSDTemplate(PrintWriter writer,String tcName,ArrayList<String>CustomPart,String tdNumber,String tdSlogan,int caseNumber){	
		String[] separated=tcName.split("_");
		String tcNumber=separated[2]; // getting the tc number from separated names
		String tcSlogan="";
		int index=3;
		
		while(index<separated.length){
			tcSlogan+=separated[index]; // after tc number there is a slogan
			if(index!=separated.length-1)
				tcSlogan+="_";
			index++;
		}
		for(String Line: CustomPart){
			writer.write(Line.replaceAll("AAAA", tdNumber)
					.replaceAll("slogan", tdSlogan)
					.replaceAll("NNN",tcNumber)
					.replaceAll("tcSlogan", tcSlogan)
					.replaceAll("\\$caseNumber", Integer.toString(caseNumber))); //replacing regular exp according to the pattern
		}
	}
	/**Method that creates files and call writeMDTemplate to fill they.
	 * @param tdslogan Slogan of TD
	 * @param tdNumber Number of TD
	 * @param tcListPath Path to the list of tc provided by user
	 * @param templatePath path to the template program is creating
	 * @param patternPath path to the pattern (default or provided by user)
	 */
	public void CreateMDTemplate(String tdslogan, String tdNumber, String tcListPath,String templatePath,String patternPath){
		
		File tcFile = new File(tcListPath);
		ArrayList<String[]> tcNames;
		try {
			tcNames = this.MDSeperateTestCases(tcFile);
		} catch (FileNotFoundException e1) {
			MessageDialog.showMessage("File containing TC's list not found");
			return;
		}
		PrintWriter writer = null;
		Scanner in = null;
		String filename="";		
		try{ // slogan in md is optional and therefore the filename changes
			if(tdslogan=="") 
				filename="WSMD"+tdNumber+"_TI.ttcn";
			else
				filename="WSMD"+tdNumber+"_"+tdslogan+"_TI.ttcn";
			writer = new PrintWriter(templatePath+"/"+filename, "UTF-8");
			if(patternPath.equals("Optional")) //Check if Default or custom pattern
				in = new Scanner(new FileReader(CoreSupport.getPath("materials/TEMPLATES/MD_TEMPLATE.ttcn"))); // default
			else // custom
				in = new Scanner(new FileReader(patternPath));
			writeMDTemplate(writer,in,tcNames,tdNumber,tdslogan); //filling the template
			CoreSupport.getFirstIProject().refreshLocal(IResource.DEPTH_INFINITE, null);// refreshing workspace
			
		} catch (FileNotFoundException e) {
			MessageDialog.showMessage("File with Template or TC's list not found");
		} catch (UnsupportedEncodingException e) {
			MessageDialog.showMessage("File with Template has wrong encoding");
		} catch (CoreException e){
			e.printStackTrace();
		}catch (NullPointerException e){
			MessageDialog.showMessage("File with Template or TC's list not found");
			}

		finally{ //close the resources
			if(in!=null)
				in.close();
			if(writer!=null){
				writer.close();
			}
		}
			openInEditor(templatePath+"/"+filename,CoreSupport.getFirstIProject()); //opening file in default editor
	}
	/**Method that fills the template with text
	 * @param writer PrintWriter of the desired to write fileto write on the desired file
	 * @param in Scanner to read text from pattern file
	 * @param tcNames ArrayList of Names of TestCases
	 * @param tdNumber Number of td
	 * @param tdSlogan Slogan of td
	 */
	private void writeMDTemplate(PrintWriter writer, Scanner in, ArrayList<String[]>tcNames, String tdNumber, String tdSlogan){
		while(in.hasNextLine())	{
			String currentLine=in.nextLine();
			
			if(currentLine.equals("#TC_START")){
				ArrayList<String>customPart=new ArrayList<String>();
					currentLine=in.nextLine();
				while(!currentLine.equals("#TC_END")){
						customPart.add(currentLine+"\n");
						currentLine=in.nextLine();
						}
				int number=1;
				for(String []tcName: tcNames){
					writeCustomTCPartforMDTemplate(writer,tcName[0], customPart,number++,tcName[1]);
				}
			}
			else
				writer.write(currentLine.replaceAll("XXXXX", tdNumber).replaceAll("_ABCDEFG", "_"+tdSlogan));
				writer.write("\n");
		}	
	}
	/**method that generates the custom to each tc's part and writes it in the file
	 * @param writer PrintWriter ,which writes on the desired file
	 * @param tcNumber Number of the tc separated by '_'
	 * @param CustomPart ArrayList of strings that create custom to all tc part
	 * @param caseNumber Number needed to replace $caseNumber in switch
	 *@param tcSlogan Slogan of the tc(Optional);
	 */
	private void writeCustomTCPartforMDTemplate(PrintWriter writer,String tcNumber,ArrayList<String>CustomPart,int caseNumber,String tcSlogan){	
		for(String Line: CustomPart){
			writer.write(Line.replaceAll("X_Y", tcNumber.substring(5))
					.replaceAll("X.Y",tcNumber.substring(5).replace("_", "."))
					.replaceAll("\\$caseNumber", Integer.toString(caseNumber))
					.replaceAll("\\$Title",tcSlogan));
		}
	}
	/**Method that opens file in eclipse default editor
	 * @param filepath path to the file to open
	 * @param project project to which file belongs
	 */
	private void openInEditor(String filepath,IProject project){
		try {
		 IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		 IWorkspace workspace= ResourcesPlugin.getWorkspace();    
		 File file=new File(filepath);
		 IPath location= Path.fromOSString(file.getAbsolutePath()); //Cast file to ifile
		 IFile ifile= workspace.getRoot().getFileForLocation(location);
			IEditorDescriptor desc = PlatformUI.getWorkbench().
			        getEditorRegistry().getDefaultEditor(ifile.getName());
				page.openEditor(new FileEditorInput(ifile), desc.getId()); //open in editor
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
			} catch (PartInitException e) {
				e.printStackTrace();
			} catch (CoreException e) {
				e.printStackTrace();
			} catch(NullPointerException e){
				MessageDialog.showMessage("There is no open project in this directory to add file to.");
			}
		
	}
}