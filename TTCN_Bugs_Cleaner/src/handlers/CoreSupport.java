package handlers;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

public class CoreSupport {
	
	/**Method that gets relative path to the file and returns that file
	 * @param relativePath relative path to the file
	 * @return file to which the path leads
	 */
	public static File getFile(String relativePath) {
		
		Bundle bundle = Platform.getBundle("TTCN_Bugs_Cleaner"); //Get the plugin bundle
		URL fileURL = bundle.getEntry(relativePath);
		File file = null;
		try {
			URL resolvedFileURL = FileLocator.toFileURL(fileURL);
			// We need to use the 3-arg constructor of URI in order to properly escape file system chars
			URI resolvedURI = new URI(resolvedFileURL.getProtocol(), resolvedFileURL.getPath(), null);
			file = new File(resolvedURI);
		} 
		catch (URISyntaxException e1) {
			e1.printStackTrace();
		} 
		catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return file;
	}
	
	/**Method that changes relative file path to absolute file path
	 * @param relativePath relative path to the file
	 * @return absolute path to the file
	 */
	public static String getPath(String relativePath) {
		
		Bundle bundle = Platform.getBundle("TTCN_Bugs_Cleaner");
		URL fileURL = bundle.getEntry(relativePath);
		File file = null;
		try {
			URL resolvedFileURL = FileLocator.toFileURL(fileURL);
			
			// We need to use the 3-arg constructor of URI in order to properly escape file system chars
			URI resolvedURI = new URI(resolvedFileURL.getProtocol(), resolvedFileURL.getPath(), null);
			file = new File(resolvedURI);
		} 
		catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return file.toString();
		
	}
	
	/**Method that returns path to the Workspace of eclipse
	 * @return path to eclipse's workspace
	 */
	public static String getWorkspacePath(){
		return ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
	}
	
	/**Method that returns path to the first open project
	 * @return path to eclipse's first open project
	 * @throws NullPointerException exception thrown when there is no open project
	 */
	public static String getFirstProjectPath()throws NullPointerException{
		
		IProject[]projects=ResourcesPlugin.getWorkspace().getRoot().getProjects();
		for(IProject project: projects)
			
			if( project.isOpen() )
				return project.getLocation().toString();
		
		throw new NullPointerException(); // if there is no open project throw an exception

	}
	
	/**Method that returns first open Iproject
	 * @return first open Iproject
	 * @throws NullPointerException exception thrown when there is no open project
	 */
	public static IProject getFirstIProject()throws NullPointerException{
		
		IProject[]projects=ResourcesPlugin.getWorkspace().getRoot().getProjects();
		for(IProject project: projects)
			
			if(project.isOpen())
				return project;
		
		throw new NullPointerException(); // if there is no open project throw an exception
	}
	
	/**Method that returns path to the folder.If the folder does not exist it creates one.
	 * @param folderName name of the folder
	 * @param Path path in which folder is(or will be)
	 * @return path to already existing or newly created folder
	 */
	public static String getFolder( String folderName, String Path ){
		
		File theDir=new File(Path+"/"+folderName);
		if(!theDir.exists())
			theDir.mkdir();
		
		return Path+"/"+folderName;
	}
}