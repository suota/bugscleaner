package launchers;
import java.util.ArrayList;

/**
 * Class for distributing ttcn files with scripts.
 *
 */
public class DistributorLauncher {
	private String pythonPath;  
	private String perlPath; 
	private String ttcnFile; //array of ttcn files to check
	private String[] scriptArray ; //array of chosen scripts by user
	private ArrayList<String[]> errors; 
	private ArrayList<String[]> warnings;
	
	/**
	 * Constructor of the class
	 * 
	 * @param pythonPath Path to the Python executable file
	 * @param perlPath Path to the Perl executable file
	 * @param ttcnArray Array of ttcn scripts which we want to launch scripts on.
	 * @param scriptArray Array of chosen scripts which we want to use.
	 * 
	 */
	
	public DistributorLauncher(String pythonPath, String perlPath, String ttcnFile, String[] scriptArray){
		this.perlPath = perlPath;
		this.pythonPath = pythonPath;
		this.scriptArray = scriptArray;
		this.ttcnFile = ttcnFile;	
	}


	/**
	 * Method which create object of the class Launcher and uses it to launch all chosen scripts on all ttcn files.
	 * 
	 * @return ArrayList of Strings with all detected errors.
	 * 
	 */
	public ArrayList<String[]> distribute() { //method which use Launcher to start adequate scripts
		
		Launcher launcher = new Launcher();
		errors = new ArrayList <String[]>();
		warnings = new ArrayList<String[]>(); //array of warnings which will be shown to user
		
			for( int j = 0 ; j< scriptArray.length ; j++){
				
				String ending = new String(scriptArray[j].substring(scriptArray[j].length() -3)); // variable ending is an extension of script
				
				if (ending.equals(".py")){ 		//starting python script
					launcher.launchScript(pythonPath, scriptArray[j], ttcnFile, warnings, errors);
				}
				else if (ending.equals(".pl")){ 	//starting perl script
					launcher.launchScript(perlPath, scriptArray[j], ttcnFile, warnings, errors);
				}
				else {
					System.out.println("Wrong script extension! Acceptable only .py or .pl");
				}
			}
		
		return warnings;
	}
	
	public ArrayList<String[]> getErrors() {
		return errors;
	}
}
