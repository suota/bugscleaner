package launchers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class which has a method to launch sripts on ttcn files
 */

public class Launcher {
	
	/**
	 * Method to launch single script on the single ttcn file
	 * 
	 * @param path Path to the Perl or Python executable file
	 * @param scriptName The name of the script which will be launched
	 * @param ttcnName The name of the ttcn file which will be checked by script
	 * 
	 * @return The array of numbers of lines where script found errors
	 */
	
	public void launchScript(String path, String scriptName, String ttcnName, ArrayList <String[]> warnings,
			ArrayList <String[]> errors){
		

		String [] command = {path, scriptName, ttcnName}; //preparing a line for a system
		ProcessBuilder pb = new ProcessBuilder(command); //creating new system process to start script
		Process p;
		ArrayList<String> returned = new ArrayList<String>();
		
		try{
			p = pb.start();
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
			String line= in.readLine();
				while(line != null){ //reading lines returned by script
					returned.add(line);
					line = in.readLine();
				}
				
				//returned.add(scriptName);		
		} catch (IOException e){
			System.out.println(e);
		}
		
		int index;
		if (returned.indexOf("Lines:") != -1){
			index = returned.indexOf("Lines:");
			ArrayList<String> lineNumbers = new ArrayList<String>(returned.subList(index + 1, returned.size()));
			String[] lines = lineNumbers.toArray(new String[lineNumbers.size()]);
			for(int k=0; k<lines.length; k++){
				String line = lines[k];
				String lineNumber = line.split(" ")[0];
				String [] elements = new String[4]; //array for all information of single warning
				elements[0] = ttcnName;
				elements[1] = scriptName;
				elements[2] = lineNumber;
				elements[3] = line.substring(line.indexOf(" ") + 1, line.length());
				warnings.add(elements); // adding new warning to the main list
			}
		}
		else{
			String [] elements = new String[3];
			elements[0] = ttcnName;
			elements[1] = scriptName;
			if (returned.indexOf("Error:") != -1){
				index = returned.indexOf("Error:");
				elements[2] = returned.get(index + 1);
			}
			else {
				elements[2] = "Fundamental error: Wrong script or wrong path";
			}
			errors.add(elements);
		}
		
	 }
}